<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TodaysSalesCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return [
            'data' => $this->collection->map(function($data) {
                return [
			"id" => 226,
			"shop_id"=> 1,
			"branch_id"=> 1,
			"transaction_type"=> 3,
			"transaction_id"=> 871549,
			"payment_method"=> 1,
			"payment_date"=> "2022-08-26",
			"total_amount"=> $data->grand_total,
			"discount"=> $data->coupon_discount,
			"paid_amount"=> $data->paid_amount,
			"due_amount"=> $data->due_amount,
			"bank_account"=> null,
			"account_number"=> null,
			"card_holder"=> null,
			"card_number"=> 0,
			"created_by"=> 1,
			"updated_by"=> 0,
			"deleted_by"=> 0,
			"created_at"=> $data->created_at,
			"updated_at"=> $data->updated_at
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
