<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TodaysPurchaseCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return [
            'data' => $this->collection->map(function($data) {
                return [
			"id"=> 77,
			"shop_id"=> 1,
			"purchase_id"=> 120220310160726,
			"category_id"=> 0,
			"product_id"=> 1150,
			"opening"=> 0,
			"purchased_qty"=> 5,
			"unit_price"=> 1.5,
			"vat"=> 0,
			"total_price"=> 7.5,
			"lot_no"=> "0",
			"expiry_date"=> "2022-08-31",
			"manufacture_date"=> "2022-08-01",
			"comments"=> null,
			"status"=> 1,
			"created_by"=> 30,
			"updated_by"=> 0,
			"deleted_by"=> 0,
			"created_at"=> "2022-08-29 12=>31=>15",
			"updated_at"=> "2022-08-29 12=>31=>15",
			"purchaseApprovedID"=> 136,
			"invoice_no"=> "715",
			"invoice_date"=> "2022-08-29",
			"challan_no"=> null,
			"challan_date"=> null,
			"supplier_id"=> 8,
			"purchased_by"=> 30,
			"purchased_date"=> "2022-08-29",
			"purchase_remarks"=> "",
			"month"=> 8,
			"year"=> 2022,
			"sub_total"=> 12.5,
			"discount"=> 2,
			"total_amount"=> 10.5,
			"paid_amount"=> 0,
			"due_amount"=> 10.5,
			"paid_status"=> 0,
			"is_due"=> 0,
			"approve_status"=> 1,
			"purchaseId"=> 120220310160726,
			"purchaseDiscount"=> 2,
			"purchaseDue"=> 10.5,
			"product_name"=> $data->productname,
			"approvedStatus"=> 1
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
