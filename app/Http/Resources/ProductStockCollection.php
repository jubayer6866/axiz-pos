<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductStockCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return [
            'data' => $this->collection->map(function($data) {
                return [
                    "id"=> 2369,
                    "shop_id"=> 1,
                    "product_id"=> 2369,
                    "quantity"=> -5,
                    "expiry_date"=> $data->expiry_date,
                    "last_modified"=> "2020-11-24 02=>36=>10",
                    "created_by"=> 27,
                    "updated_by"=> 30,
                    "deleted_by"=> 0,
                    "created_at"=> "2020-10-08 17=>11=>19",
                    "updated_at"=> "2020-11-10 04=>06=>40",
                    "barcode"=> "135-0080-078",
                    "generic_name"=> "Nicotinamide + Pyridoxine Hydrochloride + Riboflavin + Vitamin B1 + Zinc",
                    "name"=> $data->name,
                    "code"=> "135-0080-078",
                    "box_size"=> null,
                    "strength"=> "20 mg",
                    "dosage_form"=> 5,
                    "unit"=> null,
                    "rack"=> 1,
                    "group"=> 1,
                    "category"=> 2,
                    "dar"=> "135-0080-078",
                    "manufacturer"=> 16,
                    "supplier_id"=> 16,
                    "trade_price"=> "43",
                    "vat_amount"=> 0,
                    "total_tp"=> 5,
                    "sell_price"=> 50,
                    "min_order_level"=> null,
                    "tax"=> 0,
                    "product_picture"=> "default.png",
                    "side_effects"=> null,
                    "description"=> null,
                    "dosageForm"=> "Syrup",
                    "location"=> "Rack 1",
                    "category_name"=> "Prescription Only Medicine",
                    "manufact_name"=> "APC Pharma Limited"
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
