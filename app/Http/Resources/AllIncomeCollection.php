<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AllIncomeCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        
            return $this->collection->map(function($data) {

                return [
                    "incomeHeadName"=>$data->name,
                    "transMode"=> $data->payment_type,
                    "id"=> 9,
                    "shop_id"=> 1,
                    "branch_id"=> 1,
                    "income_head"=> 15,
                    "received_date"=> "2021-10-31",
                    "received_by"=> 31,
                    "received_mode"=> 19,
                    "invoice_no"=> "434",
                    "invoice_date"=> "2021-10-31",
                    "received_from"=> "Mr. X",
                    "received_amount"=> 200,
                    "purpose"=> "Doctor Visit",
                    "notes"=> null,
                    "month"=> 10,
                    "year"=> 2021,
                    "created_by"=> 31,
                    "deleted_by"=> 0,
                    "updated_by"=> 0,
                    "created_at"=> "2021-10-31 12:08:16",
                    "updated_at"=> "2021-10-31 12:08:16"
                ];
            });
        
    }

  
}
