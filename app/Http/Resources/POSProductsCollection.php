<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class POSProductsCollection extends ResourceCollection
{
    
    public function with($request)
    {
        $data = $this->collection[0];

        return [
            'success' => true,
            'status' => 200,
            'total_customers' => sizeof($data["customers"]), 
            'total_products' => sizeof($data["get_products"]) 
        ];
    }
    
    public function toArray($request)
    {   
        $data1 = $this->collection[0];
        return [
            'data' => [
                 
                  'customer' => $data1["customers"]->map(function($custdata){
                      return [
                        'id' => $custdata->id,
                        'customer_name' => $custdata->name,
                        'mobile' => "not find",
      
                      ];
                  }),
                  'medicine_list' => $data1["get_products"]->map(function($proddata){

                    $stock_info = " ";
                    if($proddata->variant_product) {
                        foreach ($proddata->stocks as $key => $stock) {
                     $stock_in = $stock->variant.' / '.$stock->qty.' / '.$stock->price.'//';
                     $stock_info .= $stock_in;
                        }
                    } else {
                        $stock_info = "none";
                    }

                    // $batches =json_decode($proddata->batches);
                    $stock_id = $proddata->stock_id;

                    return [
                    'stock_id' => $proddata->stock_id,
                     'unit_price' => $proddata->price,
                      'product_id' => $proddata->id,
                      'product_name' => $proddata->name,
                      'shop_id' => $proddata->shop_id,
                      'strength' => $proddata->variant,
                      'available_stock' => $proddata->qty,
                      'brand' => $proddata->brand_name,
                      'expire_date' => "not find",
                      'unit_id' => 0,
                      'category_id' => $proddata->catid,
                      'category_name' => $proddata->category_name,
                      'vat_amount' => 00,
                      'unit_name' => $proddata->unit,
                      'generic_name' => "not find",
                      'dosageName' => $stock_info,
                      'manufact_name' => $proddata->brand_name,
                      'batch_no' =>  $proddata->batch_no,
                      
                        // collect($batches)->map(function($batchData) use ($stock_id){
                        //     return [

                        //         'stock_id' => $stock_id,
                        //         'batch_no' => $batchData->batch_no,
                        //         'quantity' => $batchData->quantity,
                        //         'expiry_date' => $batchData->expiry_date,
                        //         'unit_price' => $batchData->unit_price,
                        //         'damage_status' => $batchData->damage_status
                        //     ];
                        // })

                      
                    ];
                })
                  
            ]   
        ];  
    }

   
}
