<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AllExpenceCollecton extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return 
             $this->collection->map(function($data) {

                return [
                    "expenseHeadName"=>$data->purchase_no,
                    "transMode"=> $data->remarks,
                    "id"=> 5,
                    "shop_id"=> 1,
                    "branch_id"=> 1,
                    "expense_head"=> 17,
                    "expense_date"=> "2021-10-28",
                    "expense_by"=> 31,
                    "account_manager"=> 1,
                    "vendor"=> 1,
                    "expense_mode"=> 20,
                    "bank_account"=> 1223,
                    "cheque_no"=> 211,
                    "invoice_no"=> 1,
                    "invoice_date"=> "2021-10-28",
                    "quantity"=> 1,
                    "expense_amount"=> 2000,
                    "purpose"=> null,
                    "notes"=> null,
                    "payment_status"=> 1,
                    "month"=> 10,
                    "year"=> 2021,
                    "created_by"=> 31,
                    "deleted_by"=> 0,
                    "updated_by"=> 0,
                    "created_at"=> "2021-10-28 12:41:35",
                    "updated_at"=> "2021-10-28 12:41:35"
                ];
            });
        
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
