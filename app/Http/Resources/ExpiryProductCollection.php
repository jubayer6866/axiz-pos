<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ExpiryProductCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return [
            'data' => $this->collection->map(function($data) {
                return [
                    "purchase_id"=> 120201123131010,
                    "productName"=> $data->name,
                    "generic_name"=> "Omeprazole",
                    "strength"=> $data->unit,
                    "dosageName"=> "Capsule",
                    "batch_no"=> "22",
                    "expiry_date"=> "2020-11-23",
                    "quantity"=> 401
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
