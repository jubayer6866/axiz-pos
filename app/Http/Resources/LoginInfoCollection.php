<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LoginInfoCollection extends ResourceCollection
{
    
    public function toArray($request)
    {
        
        $data1 = $this->collection[0];
        $userdata = $data1['user'];
        $shopdata = $data1['shop'];
     
        return [
            'data'=>[
                'userInfo' =>  [
                        "id" => $userdata->id,
			            "shop_id" =>empty($shopdata->id)? 1 : $shopdata->id,
			            "firstname" => $userdata->name,
			            "lastname" => null,
			            "mobile" => null,
			            "email" => $userdata->email, 
                ],
                

                'shopInfo' => [
                        "id"=>  empty($shopdata->id)? 1 : $shopdata->id,
                        "shop_type"=>  0,
                        "name"=>  empty($shopdata->name)? "In House":$shopdata->name,
                        "code"=>  empty($shopdata->code)? "0.00":$shopdata->code,
                        "regi_no"=>  "000000",
                        "establishment"=>  2010,
                        "email"=>  empty($shopdata->email)? $userdata->email:$shopdata->email,
                        "contact_no"=>  "01990000001",
                        "website"=>  null,
                        "status"=>  1,
                        "state"=>  "Accepted",
                        "about"=>  null,
                        "division_id"=>  1,
                        "district_id"=>  1,
                        "upazila_id"=>  1,
                        "union_id"=>  2,
                        "road"=>  "Test",
                        "house"=>  null,
                        "location"=>  null,
                        "post_office"=>  null,
                        "postcode"=>  1207,
                        "logo"=>  "default.png",
                        "created_by"=>  27,
                        "updated_by"=>  27,
                        "deleted_by"=>  0,
                        "created_at"=> empty($shopdata->created_at)? null : $shopdata->created_at,
                        "updated_at"=>  empty($shopdata->updated_at)? null : $shopdata->updated_at,
                        "ownername"=>  empty($shopdata->user->name)? $userdata->name : $shopdata->user->name,
                        "ownercontact"=>  "01900000011",
                        "owneremail"=>   empty($shopdata->email)? $userdata->email : $shopdata->user->email,
                        "pharmachist_name"=>  "test",
                        "pharmachist_regno"=>  "test-02323",
                        "username"=>  empty($shopdata->user->name)? $userdata->name : $shopdata->user->name,
                        "password"=>  "123456"
                ],   

            ]
        ];
    }

    public function with($request)
    {
        return [
            'status'    =>  200,
            'token'     => $this->collection[0]["token"],
            'message'   => "Login successful"
        ];
    }
}
