<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RecentSalesCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return 
             $this->collection->map(function($data) {

                return [
                    "id"=> 219,
                    "shop_id"=> 1,
                    "sales_id"=> 7606410,
                    "sales_type"=> 2,
                    "sales_barcode"=> "",
                    "invoice_date"=> "2022-08-29",
                    "customer_id"=> 65,
                    "invoice_detail"=> "",
                    "total_discount"=> 2,
                    "sub_total"=> 23,
                    "invoice_discount"=> 0,
                    "grand_total"=> 23,
                    "paid_amount"=> 23,
                    "due_amount"=> 0,
                    "is_due"=> 0,
                    "due_payment_date"=> "2022-08-29",
                    "change_amount"=> 0,
                    "created_by"=> 1,
                    "month"=> 8,
                    "year"=> 2022,
                    "hold_title"=> null,
                    "hold_status"=> 0,
                    "updated_by"=> 1,
                    "deleted_by"=> 0,
                    "created_at"=> "2022-08-29 12:29:35",
                    "updated_at"=> "2022-08-29 12:29:35",
                    "purchase_upload"=> null,
                    "customerName"=> "Nurul vai"
                ];
            });
        
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
