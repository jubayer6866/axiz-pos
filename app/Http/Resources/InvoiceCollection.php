<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Utility\CategoryUtility;

class InvoiceCollection extends ResourceCollection
{
    public function toArray($request)
    {
       dd($this->collection);
        return [
            'data' => $this->collection->map(function($data) {
                
                return [
                    
                    'invoice_no' => $data->code,
                    'invoice_date' => $data->created_at,
                    'customer_name' => $data->name,
                    'phone' => $data->phone,
                    'total' => $data->grand_total,
                    'paid' => 0.00,
                    'due' => 0.00,
                    'due_payment_date' => $data->created_at,
                    'product_name' => $data->product_name,
                    'strength' => $data->unit,
                    'quantity' => $data->quantity,
                    'unit_price' => $data->price,
                    'discount' => $data->coupon_discount,
                    'total_price' => $data->grand_total,
                    
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
