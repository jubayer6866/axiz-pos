<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductSaleCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return 
             $this->collection->map(function($data) {
            
                return [
                    
                    "product_id"=> $data->id,
                    "product_name"=> $data->name,
                    "generic_name"=> $data->brand->name,
                    "dosageName"=> $data->name,
                    "is_supplier"=> 1,
                    "strength"=> $data->unit,
                    "min_order_level"=> $data->min_qty,
                    "trade_price"=> $data->purchase_price,
                    "supplier_id"=> $data->supplier_id,
                    "sell_price"=> $data->unit_price,
                    "quantity"=> $data->current_stock,
                    "last_modified"=> $data->updated_at,
                    "category_name"=> $data->category->name,
                    "available_stock"=> $data->current_stock
                ];
            });
        
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
