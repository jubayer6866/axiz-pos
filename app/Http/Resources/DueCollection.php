<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DueCollection extends ResourceCollection
{
    public function toArray($request)
    {
        //dd($this->collection);
        return [
            'data' => $this->collection->map(function($data) {

                return [
                        "id"=> 57,
                        "shop_id"=> 1,
                        "sales_id"=> 441922,
                        "sales_type"=> 2,
                        "sales_barcode"=> "",
                        "invoice_date"=> "2022-08-16",
                        "customer_id"=> 57,
                        "invoice_detail"=> "",
                        "total_discount"=> 0,
                        "sub_total"=> 0,
                        "invoice_discount"=> 0,
                        "grand_total"=> 5845412,
                        "paid_amount"=> null,
                        "due_amount"=> 125,
                        "is_due"=> 1,
                        "due_payment_date"=> "2022-08-16",
                        "change_amount"=> 0,
                        "created_by"=> 1,
                        "month"=> 8,
                        "year"=> 2022,
                        "hold_title"=> null,
                        "hold_status"=> 0,
                        "updated_by"=> 1,
                        "deleted_by"=> 0,
                        "created_at"=> "2021-10-31 12:08:16",
                        "updated_at"=> "2021-10-31 12:08:16",
                        "purchase_upload"=> null,
                        "customer_name"=> $data->name,
                        "code"=> null,
                        "mobile"=> "01718837689",
                        "email"=> null,
                        "address"=> null
                ];
            })
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
            'status' => 200
        ];
    }
}
