<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AffiliateController;
use App\Http\Controllers\OTPVerificationController;
use Illuminate\Http\Request;
use App\Http\Controllers\ClubPointController;
use App\Models\Order;
use App\Models\Supplier;
use App\Models\Supplier_ledger;
use App\Models\Wearhouse;
use App\Models\Purchase_order;
use App\Models\Purchase_order_item;
use App\Models\Cart;
use App\Models\Address;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\CommissionHistory;
use App\Models\Color;
use App\Models\OrderDetail;
use App\Models\CouponUsage;
use App\Models\Coupon;
use App\OtpConfiguration;
use App\Models\User;
use App\Models\BusinessSetting;
use App\Models\CombinedOrder;
use App\Models\SmsTemplate;
use App\Models\DeliveryBoyController;
use Auth;
use Session;
use DB;
use Mail;
use App\Mail\InvoiceEmailManager;
use App\Utility\NotificationUtility;
use CoreComponentRepository;
use App\Utility\SmsUtility;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Redirect;

class OrderController extends Controller
{
    // All Orders
    public function all_orders(Request $request)
    {
        CoreComponentRepository::instantiateShopRepository();
        
        $date = $request->date;
        $sort_search = null;
        $delivery_status = null;
        $payment_status = '';
        
        $orders = Order::orderBy('id', 'desc');
        $admin_user_id = User::where('user_type', 'admin')->first()->id;
        if(Route::currentRouteName() == 'inhouse_orders.index') {
            $orders = $orders->where('orders.seller_id', '=', $admin_user_id);
        }
        if(Route::currentRouteName() == 'seller_orders.index') {
            $orders = $orders->where('orders.seller_id', '!=', $admin_user_id);
        }
        if(Route::currentRouteName() == 'pick_up_point.index') {
            $orders->where('shipping_type', 'pickup_point')->orderBy('code', 'desc');
            if (Auth::user()->user_type == 'staff' && Auth::user()->staff->pick_up_point != null) {
                $orders->where('shipping_type', 'pickup_point')
                        ->where('pickup_point_id', Auth::user()->staff->pick_up_point->id);
            }
        }
        if ($request->search) {
            $sort_search = $request->search;
            $orders = $orders->where('code', 'like', '%' . $sort_search . '%');
        }
        if ($request->payment_status != null) {
            $orders = $orders->where('payment_status', $request->payment_status);
            $payment_status = $request->payment_status;
        }
        if ($request->delivery_status != null) {
            $orders = $orders->where('delivery_status', $request->delivery_status);
            $delivery_status = $request->delivery_status;
        }
        if ($date != null) {
            $orders = $orders->where('created_at', '>=', date('Y-m-d', strtotime(explode(" to ", $date)[0])).'  00:00:00')
            ->where('created_at', '<=', date('Y-m-d', strtotime(explode(" to ", $date)[1])).'  23:59:59');
        }
        $orders = $orders->paginate(15);
        return view('backend.sales.index', compact('orders', 'sort_search', 'payment_status', 'delivery_status', 'date'));
    }

    public function show($id)
    {
        $order = Order::findOrFail(decrypt($id));
        $order_shipping_address = json_decode($order->shipping_address);
        $delivery_boys = User::where('city', $order_shipping_address->city)
            ->where('user_type', 'delivery_boy')
            ->get();

        $order->viewed = 1;
        $order->save();
        return view('backend.sales.show', compact('order', 'delivery_boys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carts = Cart::where('user_id', Auth::user()->id)
            ->get();

        if ($carts->isEmpty()) {
            flash(translate('Your cart is empty'))->warning();
            return redirect()->route('home');
        }

        $address = Address::where('id', $carts[0]['address_id'])->first();

        $shippingAddress = [];
        if ($address != null) {
            $shippingAddress['name']        = Auth::user()->name;
            $shippingAddress['email']       = Auth::user()->email;
            $shippingAddress['address']     = $address->address;
            $shippingAddress['country']     = $address->country->name;
            $shippingAddress['state']       = $address->state->name;
            $shippingAddress['city']        = $address->city->name;
            $shippingAddress['postal_code'] = $address->postal_code;
            $shippingAddress['phone']       = $address->phone;
            if ($address->latitude || $address->longitude) {
                $shippingAddress['lat_lang'] = $address->latitude . ',' . $address->longitude;
            }
        }

        $combined_order = new CombinedOrder;
        $combined_order->user_id = Auth::user()->id;
        $combined_order->shipping_address = json_encode($shippingAddress);
        $combined_order->save();

        $seller_products = array();
        foreach ($carts as $cartItem){
            $product_ids = array();
            $product = Product::find($cartItem['product_id']);
            if(isset($seller_products[$product->user_id])){
                $product_ids = $seller_products[$product->user_id];
            }
            array_push($product_ids, $cartItem);
            $seller_products[$product->user_id] = $product_ids;
        }

        foreach ($seller_products as $seller_product) {
            $order = new Order;
            $order->combined_order_id = $combined_order->id;
            $order->user_id = Auth::user()->id;
            $order->shipping_address = $combined_order->shipping_address;

            $order->additional_info = $request->additional_info;
            
            $order->shipping_type = $carts[0]['shipping_type'];
            if ($carts[0]['shipping_type'] == 'pickup_point') {
                $order->pickup_point_id = $cartItem['pickup_point'];
            }
            $order->payment_type = $request->payment_option;
            $order->delivery_viewed = '0';
            $order->payment_status_viewed = '0';
            $order->code = date('Ymd-His') . rand(10, 99);
            $order->date = strtotime('now');
            $order->save();

            $subtotal = 0;
            $tax = 0;
            $shipping = 0;
            $coupon_discount = 0;

            //Order Details Storing
            foreach ($seller_product as $cartItem) {
                $product = Product::find($cartItem['product_id']);

                $subtotal += cart_product_price($cartItem, $product, false, false) * $cartItem['quantity'];
                $tax +=  cart_product_tax($cartItem, $product,false) * $cartItem['quantity'];
                $coupon_discount += $cartItem['discount'];

                $product_variation = $cartItem['variation'];

                $product_stock = $product->stocks->where('variant', $product_variation)->first();
                if ($product->digital != 1 && $cartItem['quantity'] > $product_stock->qty) {
                    flash(translate('The requested quantity is not available for ') . $product->getTranslation('name'))->warning();
                    $order->delete();
                    return redirect()->route('cart')->send();
                } elseif ($product->digital != 1) {
                    $product_stock->qty -= $cartItem['quantity'];
                    $product_stock->save();
                }

                $order_detail = new OrderDetail;
                $order_detail->order_id = $order->id;
                $order_detail->seller_id = $product->user_id;
                $order_detail->product_id = $product->id;
                $order_detail->variation = $product_variation;
                $order_detail->price = cart_product_price($cartItem, $product, false, false) * $cartItem['quantity'];
                $order_detail->tax = cart_product_tax($cartItem, $product,false) * $cartItem['quantity'];
                $order_detail->shipping_type = $cartItem['shipping_type'];
                $order_detail->product_referral_code = $cartItem['product_referral_code'];
                $order_detail->shipping_cost = $cartItem['shipping_cost'];

                $shipping += $order_detail->shipping_cost;
                //End of storing shipping cost

                $order_detail->quantity = $cartItem['quantity'];
                $order_detail->save();

                $product->num_of_sale += $cartItem['quantity'];
                $product->save();

                $order->seller_id = $product->user_id;

                if ($product->added_by == 'seller' && $product->user->seller != null){
                    $seller = $product->user->seller;
                    $seller->num_of_sale += $cartItem['quantity'];
                    $seller->save();
                }

                if (addon_is_activated('affiliate_system')) {
                    if ($order_detail->product_referral_code) {
                        $referred_by_user = User::where('referral_code', $order_detail->product_referral_code)->first();

                        $affiliateController = new AffiliateController;
                        $affiliateController->processAffiliateStats($referred_by_user->id, 0, $order_detail->quantity, 0, 0);
                    }
                }
            }

            $order->grand_total = $subtotal + $tax + $shipping;

            if ($seller_product[0]->coupon_code != null) {
                // if (Session::has('club_point')) {
                //     $order->club_point = Session::get('club_point');
                // }
                $order->coupon_discount = $coupon_discount;
                $order->grand_total -= $coupon_discount;

                $coupon_usage = new CouponUsage;
                $coupon_usage->user_id = Auth::user()->id;
                $coupon_usage->coupon_id = Coupon::where('code', $seller_product[0]->coupon_code)->first()->id;
                $coupon_usage->save();
            }

            $combined_order->grand_total += $order->grand_total;

            $order->save();
        }

        $combined_order->save();

        $request->session()->put('combined_order_id', $combined_order->id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function puracher_edit($id)
    {
        $purchase = Purchase_order::findOrFail($id);
        $purchase_item = Purchase_order_item::where('po_id', $id)->get();
        //dd($purchase_item);
        $products = Product::all();
        $supplier = Supplier::all();
        $title =  'Purchase Edit';
        $wearhouses = Wearhouse::all();
            return view('backend.purchase_order.edit', compact('products', 'title', 'supplier', 'purchase','purchase_item','wearhouses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function puracher_edit_store(Request $request)
    {
        if (!empty($request->purchase_no)) { 
            $purchase = Purchase_order::findOrFail($request->po_id);

            $purchase->purchase_no = $request->purchase_no;
            $purchase->supplier_id = $request->supplier_id;
            $purchase->date =  $request->purchase_date;
            $purchase->total_value =  $request->total;
            $purchase->created_by = Auth::user()->id;
            $purchase->remarks = $request->remarks;
            if ($purchase->save()) {
            
                Purchase_order_item::where('po_id', $request->po_id)->delete();
                
                foreach ($request->product as $key => $prod) {
                    $item = new Purchase_order_item();
                    $item->po_id = $purchase->id;
                    $item->product_id = $prod;
                    $item->qty = $request->qty[$key];
                    $item->desc = $request->desc[$key];
                    $item->price = $request->price[$key];
                    $item->amount = $request->price[$key] * $request->qty[$key];
                    $item->save();
                    
                        $p = Product::find($prod);
                        $p->purchase_price = $request->price[$key];
                        $p->save();
                }
                Supplier_ledger::where(array('supplier_id'=>$request->supplier_id,'purchase_id'=>$purchase->id,'type'=>'Purchase','descriptions'=>'Purchase Order'))->update(array('debit'=>$request->total));

                return redirect('admin/purchase_orders');
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        if ($order != null) {
            foreach ($order->orderDetails as $key => $orderDetail) {
                try {

                    $product_stock = ProductStock::where('product_id', $orderDetail->product_id)->where('variant', $orderDetail->variation)->first();
                    if ($product_stock != null) {
                        $product_stock->qty += $orderDetail->quantity;
                        $product_stock->save();
                    }

                } catch (\Exception $e) {

                }

                $orderDetail->delete();
            }
            $order->delete();
            flash(translate('Order has been deleted successfully'))->success();
        } else {
            flash(translate('Something went wrong'))->error();
        }
        return back();
    }

    public function bulk_order_delete(Request $request)
    {
        if ($request->id) {
            foreach ($request->id as $order_id) {
                $this->destroy($order_id);
            }
        }

        return 1;
    }

    public function order_details(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->save();
        return view('seller.order_details_seller', compact('order'));
    }

    public function update_delivery_status(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->delivery_viewed = '0';
        $order->delivery_status = $request->status;
        $order->save();

        if ($request->status == 'cancelled' && $order->payment_type == 'wallet') {
            $user = User::where('id', $order->user_id)->first();
            $user->balance += $order->grand_total;
            $user->save();
        }

        if (Auth::user()->user_type == 'seller') {
            foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key => $orderDetail) {
                $orderDetail->delivery_status = $request->status;
                $orderDetail->save();

                if ($request->status == 'cancelled') {
                    $variant = $orderDetail->variation;
                    if ($orderDetail->variation == null) {
                        $variant = '';
                    }

                    $product_stock = ProductStock::where('product_id', $orderDetail->product_id)
                        ->where('variant', $variant)
                        ->first();

                    if ($product_stock != null) {
                        $product_stock->qty += $orderDetail->quantity;
                        $product_stock->save();
                    }
                }
            }
        } else {
            foreach ($order->orderDetails as $key => $orderDetail) {

                $orderDetail->delivery_status = $request->status;
                $orderDetail->save();

                if ($request->status == 'cancelled') {
                    $variant = $orderDetail->variation;
                    if ($orderDetail->variation == null) {
                        $variant = '';
                    }

                    $product_stock = ProductStock::where('product_id', $orderDetail->product_id)
                        ->where('variant', $variant)
                        ->first();

                    if ($product_stock != null) {
                        $product_stock->qty += $orderDetail->quantity;
                        $product_stock->save();
                    }
                }

                if (addon_is_activated('affiliate_system')) {
                    if (($request->status == 'delivered' || $request->status == 'cancelled') &&
                        $orderDetail->product_referral_code) {

                        $no_of_delivered = 0;
                        $no_of_canceled = 0;

                        if ($request->status == 'delivered') {
                            $no_of_delivered = $orderDetail->quantity;
                        }
                        if ($request->status == 'cancelled') {
                            $no_of_canceled = $orderDetail->quantity;
                        }

                        $referred_by_user = User::where('referral_code', $orderDetail->product_referral_code)->first();

                        $affiliateController = new AffiliateController;
                        $affiliateController->processAffiliateStats($referred_by_user->id, 0, 0, $no_of_delivered, $no_of_canceled);
                    }
                }
            }
        }
        if (addon_is_activated('otp_system') && SmsTemplate::where('identifier', 'delivery_status_change')->first()->status == 1) {
            try {
                SmsUtility::delivery_status_change(json_decode($order->shipping_address)->phone, $order);
            } catch (\Exception $e) {

            }
        }

        //sends Notifications to user
        NotificationUtility::sendNotification($order, $request->status);
        if (get_setting('google_firebase') == 1 && $order->user->device_token != null) {
            $request->device_token = $order->user->device_token;
            $request->title = "Order updated !";
            $status = str_replace("_", "", $order->delivery_status);
            $request->text = " Your order {$order->code} has been {$status}";

            $request->type = "order";
            $request->id = $order->id;
            $request->user_id = $order->user->id;

            NotificationUtility::sendFirebaseNotification($request);
        }


        if (addon_is_activated('delivery_boy')) {
            if (Auth::user()->user_type == 'delivery_boy') {
                $deliveryBoyController = new DeliveryBoyController;
                $deliveryBoyController->store_delivery_history($order);
            }
        }
        return 1;
    }

   public function update_tracking_code(Request $request) {
        $order = Order::findOrFail($request->order_id);
        $order->tracking_code = $request->tracking_code;
        $order->save();

        return 1;
   }

   public function update_payment_status(Request $request)
   {
       $order = Order::findOrFail($request->order_id);
       
       if(!empty($order->payment_details)){
           $orderpayment = json_decode($order->payment_details);
           if(!empty($orderpayment)){
             $paid = $orderpayment->amount + $request->payment_amount;
           }
         }else{
            
           $paid = $request->payment_amount;
          
         }
        
         if($paid >=$order->grand_total){
         $status = 'Paid';
         }else{
           $status = $request->status; 
         }

       if (Auth::user()->user_type == 'seller') {
           foreach ($order->orderDetails->where('seller_id', Auth::user()->id) as $key => $orderDetail) {
               $orderDetail->payment_status = $status;
               $orderDetail->save();
           }
       } else {
           foreach ($order->orderDetails as $key => $orderDetail) {
               $orderDetail->payment_status = $status;
               $orderDetail->save();
           }
       }

       $status = $status;
       foreach ($order->orderDetails as $key => $orderDetail) {
           if ($orderDetail->payment_status == 'unpaid') {
               $status = 'unpaid';
           }
       }

       $oVal = (object)[
        'amount'=>$paid,
        'status'=>'VALID',
        'error'=>null
    ];

       $order->payment_status = $status;
       $order->payment_status_viewed = '1';
       $order->payment_status = 'Paid';
       $order->payment_details = json_encode($oVal);
       $order->save();

       if ($order->payment_status == 'Paid' || $order->payment_status == 'Partial') {
           $array['view'] = 'emails.payment';
           $array['subject'] = translate('Your order payment has been paid') . ' - ' . $order->code;
           $array['from'] = 'sales@4pos.com';
           $array['order'] = $order;
           $shipping_address = json_decode($order->shipping_address);
           if(!empty($shipping_address->email)){
               //Mail::to($shipping_address->email)->queue(new InvoiceEmailManager($array));
           }

           if(!empty($order->user_id)){
               $customer_id = $order->user_id;
           }else{
               $customer_id = $order->guest_id;
           }
           $cust_ledger = array();
           $cust_ledger['customer_id'] =$customer_id;
           $cust_ledger['order_id'] =$request->order_id;
           $cust_ledger['descriptions'] ='Cash Payment';
           $cust_ledger['type'] ='Payment';
           $cust_ledger['debit'] =0;
           $cust_ledger['credit'] =$request->payment_amount;
           $cust_ledger['date'] =date('Y-m-d',strtotime($request->payment_date));
           save_customer_ledger($cust_ledger);
       }
       return 1;
   }

    public function assign_delivery_boy(Request $request)
    {
        if (addon_is_activated('delivery_boy')) {

            $order = Order::findOrFail($request->order_id);
            $order->assign_delivery_boy = $request->delivery_boy;
            $order->delivery_history_date = date("Y-m-d H:i:s");
            $order->save();

            $delivery_history = \App\Models\DeliveryHistory::where('order_id', $order->id)
                ->where('delivery_status', $order->delivery_status)
                ->first();

            if (empty($delivery_history)) {
                $delivery_history = new \App\Models\DeliveryHistory;

                $delivery_history->order_id = $order->id;
                $delivery_history->delivery_status = $order->delivery_status;
                $delivery_history->payment_type = $order->payment_type;
            }
            $delivery_history->delivery_boy_id = $request->delivery_boy;

            $delivery_history->save();

            if (env('MAIL_USERNAME') != null && get_setting('delivery_boy_mail_notification') == '1') {
                $array['view'] = 'emails.invoice';
                $array['subject'] = translate('You are assigned to delivery an order. Order code') . ' - ' . $order->code;
                $array['from'] = env('MAIL_FROM_ADDRESS');
                $array['order'] = $order;

                try {
                    Mail::to($order->delivery_boy->email)->queue(new InvoiceEmailManager($array));
                } catch (\Exception $e) {

                }
            }

            if (addon_is_activated('otp_system') && SmsTemplate::where('identifier', 'assign_delivery_boy')->first()->status == 1) {
                try {
                    SmsUtility::assign_delivery_boy($order->delivery_boy->phone, $order->code);
                } catch (\Exception $e) {

                }
            }
        }

        return 1;
    }

    public function add_purchase(Request $request)
    {
        $products = Product::all();
        $supplier = Supplier::all();
        $title =  'Purchase Add';
        $wearhouses = Wearhouse::get();
        // foreach($products as $row){
        //     ProductStock::insert(['product_id'=>$row->id,'wearhouse_id'=>1,'price'=>$row->unit_price,'qty'=>$row->current_stock]);
        // }
        return view('backend.purchase_order.add', compact('products', 'title', 'supplier','wearhouses'));
    }

    public function get_puracher_product(Request $request){
        $product_id = $request->product_id;
      $product = ProductStock::where(['product_id'=>$product_id])->first();
    
        return $product;
    }


    public function store_purchase(Request $request)
    {
        if (!empty($request->purchase_no)) {
            $purchase = new Purchase_order();
            
            $purchase->purchase_no = $request->purchase_no;
            $purchase->supplier_id = $request->supplier_id;
            $purchase->purchase_from = 'from_web';
            $purchase->date =  Carbon::now();
            $purchase->total_value =  $request->total;
            $purchase->created_by = Auth::user()->id;
            $purchase->is_variation = $request->is_variation;
            $purchase->save();
            if (!empty($purchase->id)) {
              
                foreach ($request->product as $key => $prod) {
                    $item = new Purchase_order_item();
                    $item->po_id = $purchase->id;
                    $item->product_id = $prod;
                    $item->qty = $request->qty[$key];
                    if(!empty($request->batch_no[$key])){
                        $batch = $request->batch_no[$key];
                    }else{
                        $batch = 001;
                    }
                    $item->batch_no = $batch;
                    $item->expiry_date = $request->exp[$key];
                    $item->price = $request->price[$key];
                    $item->amount = $request->price[$key] * $request->qty[$key];
                    $item->save();

                    if (!empty($item->id)) {
                        $p = Product::find($prod);
                        $p->increment('current_stock', $request->qty[$key]);
						$p->purchase_price = $request->price[$key];
						$p->save();
                    
                    }
                }
                
               if(Auth::user()->user_type == 'admin'){
                return redirect()->route('purchase_orders.index');
               }else{
                return redirect()->route('purchase_list_for_purchase_executive.index');
               }
                
            } else {
                flash(translate('Something went wrong'))->error();
                return back();
            }
        } else {
            flash(translate('Something went wrong'))->error();
            return back();
        }
    }

       // All Orders
       public function purchase_orders(Request $request)
       {
           $date = $request->date;
           $sort_search = '';
           DB::enableQueryLog();
           $data = Purchase_order::select('purchase_order.*','suppliers.name')->leftjoin('suppliers', 'suppliers.id', '=', 'purchase_order.supplier_id')->orderBy('purchase_order.created_at', 'desc');
           
           if ($date != null) {
               $data = $data->where('purchase_order.created_at', '>=', date('Y-m-d 00:00:00', strtotime(explode(" to ", $date)[0])))->where('purchase_order.created_at', '<=', date('Y-m-d 23:59:59', strtotime(explode(" to ", $date)[1])));
           }
       if ($request->has('search')) {
               $sort_search = $request->search;
               $data = $data->where('purchase_no', 'like', '%' . $sort_search . '%');
           }
           //  dd(DB::getQueryLog());
           $data = $data->paginate(15);
           $title =  'Purchase Order';
           return view('backend.purchase_order.index', compact('data', 'title', 'date', 'sort_search'));
       }

       public function purchase_approve($id){
        
        $purchase = Purchase_order::findOrFail($id);
        $purchase->status = 2;
        $purchase->save();

        $purchase_order_item = Purchase_order_item::where('po_id',$id)->get();
            if(!empty($purchase_order_item)){
           
                foreach($purchase_order_item as $key => $prod){
                          $ps = ProductStock::where(['product_id'=>$prod->product_id])->get();
                          $count = 0;
                          foreach($ps as $p){

                            $resultBatch = $p->batch_no;
                            
                            if($resultBatch === $prod->batch_no){
                                    $count++;
                                    $p->increment('qty', $prod->qty);
                                    $p->expiry_date = $prod->expiry_date;
                                    $p->save();
                            }


                          }

                          if($count == 0){
                            $product_stock = new ProductStock();
                            $product_stock->product_id = $prod->product_id;
                            $product_stock->batch_no = $prod->batch_no;
                            $product_stock->expiry_date = $prod->expiry_date;
                            $product_stock->price = $prod->price;
                            $product_stock->qty =  $prod->qty;
                            $product_stock->save();
                          }
                          
                       
                }
            }
            flash(translate('Purchase status has been updated successfully!'))->success();
            return back();
        
    }

    public function purchase_orders_view($id)
    {
        $purchase = Purchase_order::join('suppliers','suppliers.id','=','purchase_order.supplier_id')->where('purchase_order.id',$id)
                    ->get();
                    
        $data_item_rows = Purchase_order_item::where('po_id', $id)
                        ->join('products','products.id','=','purchase_order_item.product_id')
                        ->get();
            return view('backend.purchase_order.view', compact('purchase','data_item_rows'));
    }


public function purchase_update_payment_status(Request $request){
    
    $purchase_id = $request->purchase_id;
    $purchase = Purchase_order::findOrFail($purchase_id);

    $purchase->payment_amount = $request->payment_amount;
    $purchase->payment_status = $request->status;
    if ($purchase->save()) {
        $supplier_ledger = new Supplier_ledger();
        $supplier_ledger->supplier_id = $purchase->supplier_id;
        $supplier_ledger->purchase_id = $purchase->id;
        $supplier_ledger->descriptions = 'Purchase Order';
        $supplier_ledger->type = 'Payment';
        $supplier_ledger->debit = 0;
        $supplier_ledger->credit = $request->payment_amount;
        $supplier_ledger->date = $request->payment_date;
        $supplier_ledger->save(); 
    }
    return 1;
}

    public function destroy_po($id)
    {
        $order = Purchase_order::findOrFail($id);

        if ($order != null) {

            $orderDetails = Purchase_order_item::where('po_id',$id)->get();
            Purchase_order_item::where('po_id', $id)->delete();
            $order->delete();
            flash(translate('Purchase Order has been deleted successfully'))->success();
        } else {
            flash(translate('Something went wrong'))->error();
        }
        return back();
    }
}
