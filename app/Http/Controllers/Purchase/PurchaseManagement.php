<?php

namespace App\Http\Controllers\Purchase;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Modules\Purchase\Purchase;
use App\Model\Modules\Transactions\PaymentTransactions;
use App\Model\Modules\Purchase\PurchaseDetail;
use App\Model\Modules\Stock\CurrentStock;
use App\Model\Modules\Product\ProductSetup;
use App\Model\Modules\Product\ProductUnits;
use App\Model\Modules\Product\ProductCategory;
use App\Model\Modules\Product\ProductGroup;
use App\Model\Modules\Product\ProductManufacturer;
use App\Model\Modules\Product\ProductOrigin;
use App\Model\Modules\Product\ProductRack;
use App\Model\Modules\Product\ProductSupplier;
use App\Model\Modules\HRM\EmployeeRecords;
use App\Model\Modules\Geo\LogPurchase;
use App\Model\Modules\Settings\Bank as BankSetup;
use App\Model\Modules\Settings\ShopInfo as ShopDetail;
use App\Model\Modules\Stock\Stocks;
use App\Model\Modules\Sales\Sales;
use Session;
use Input;
use DB;

class PurchaseManagement extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view('modules.purchase.index', [
      // 'productList'  => ProductSetup::get(),
      'categoryList' => ProductCategory::get(),
      'supplierList' => ProductSupplier::get(),
      // 'supplierList' => ProductSupplier::where('shop_id', Session::get('shop_id'))->get(),
      'bankList'     => BankSetup::get(),
      'purchasedBy'  => EmployeeRecords::whereNotIn('employee_id', [9999])->where('shop_id', Session::get('shop_id'))->get(),
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function productDetailsSearch(Request $request)
  {
    $purchaseQty = $request->get('purchaseQty');
    $unitPirce   = $request->get('unitPirce');
    $vat         = $request->get('vat');
    $paidAmount  = $request->get('paidAmount');
    $discount    = $request->get('purchase_discount');
    $productId   = $request->get('productId');

    $totalPrice = ($purchaseQty * $unitPirce) + $vat;
    // sum of total price
    $totalSummationexplode  =  $request->get('total_sum_price');
    $totalAmount     = array_sum($totalSummationexplode);

    $currentStock = CurrentStock::where('shop_id', Session::get('shop_id'))->where('product_id', $productId)->first();
    $productSetup = ProductSetup::find($productId);

    // total current stock
    $totalcurrentStock = 0;
    if (!empty($currentStock)) {
      $totalcurrentStock = $currentStock->quantity;
    }

    $data = array(
      // Total Summery
      'totalPrice'       => $totalPrice,
      'currentStock'     => $totalcurrentStock,
      'subTotal'         => $totalAmount,
      'totalAmount'      => $totalAmount - $discount,
      'totalPaidAmount'  => $totalAmount - $discount - $paidAmount,
      'totalDueAmount'   => $totalAmount - $paidAmount  - $discount,
      'unitPrice'        => !empty($productSetup->total_tp) ? $productSetup->total_tp : 0,

    );
    return json_encode($data);
  }

  /**
   * [supplyAddressSearch description]
   * @return [type] [description]
   */
  public function supplyAddressSearch($id)
  {

    $supplier = ProductSupplier::find($id);
    return $supplier->address;
  }

  /**
   * [productListByCategory description]
   * @param  [type] $supplierId [description]
   * @return [type]             [description]
   */
  public function productListByCategory($supplierId)
  {
    if ($supplierId == 10000000) {

      $productList = ProductSetup::leftjoin('product_dosage_form', 'product_add.dosage_form', 'product_dosage_form.id')
        ->select('product_add.id', 'product_add.name', 'product_add.generic_name', 'product_add.barcode', 'product_add.strength', 'product_dosage_form.name as dosageName')
        ->orderBy('product_add.name', 'asc')
        ->get();
    } else {
      $productList = ProductSetup::leftjoin('product_dosage_form', 'product_add.dosage_form', 'product_dosage_form.id')
        ->select('product_add.id', 'product_add.name', 'product_add.generic_name', 'product_add.barcode', 'product_add.strength', 'product_dosage_form.name as dosageName')
        ->where(
          [
            ['product_add.supplier_id', $supplierId],
          ]
        )->orderBy('product_add.name', 'asc')->get();
    }


    return view('modules.purchase.product-list-dropdown', [
      'productList' => $productList,
    ]);
  }

  public function purchaseList()
  {

    $type      = Input::get('type');
    $date_form = Input::get('date_form');
    $date_to   = Input::get('date_to');
    $id        = Input::get('id');
    $shops = ShopDetail::where('status', 1)->get();
    // search and vew reports condition
    $view = 'modules.purchase.purchase-list';

    if ($type == 'report') {
      $view = 'modules.reports.purchase.purchase-list-report';
    }
    $purchaseList = array();
    foreach ($shops as $shop) {
if (!preg_match('/[^A-Za-z0-9]/', $shop->code)){
      $dbname = 'pharmacy_' . $shop->code;
      \Config::set('database.connections.mysql.database', $dbname);
		\DB::purge('mysql');
        try {
		\DB::connection()->getPdo();
		} catch (\Exception $e) {
			continue;
		}
      // If date range search form summary sales
      if (!is_null($date_form) && !is_null($date_to)) {
        $e = PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
          ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
          ->leftjoin('product_supplier', 'product_supplier.id', 'product_add.supplier_id')
          ->select('purchase_detail.*',DB::raw("'$shop->name' as shop_code"),'product_supplier.fullname as supplier_name', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
          ->orderBy('purchase.created_at', 'DESC')
          ->whereBetween('purchased_date', [$date_form, $date_to])
          ->get();
      } elseif (!is_null($id)) {

        $e = PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
          ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
          ->leftjoin('product_supplier', 'product_supplier.id', 'product_add.supplier_id')
          ->select('purchase_detail.*',DB::raw("'$shop->name' as shop_code"),'product_supplier.fullname as supplier_name', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
          ->orderBy('purchase.created_at', 'DESC')
          ->where('purchase.supplier_id', $id)
          ->get();
      } else {
        $e = PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
          ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
          ->leftjoin('product_supplier', 'product_supplier.id', 'product_add.supplier_id')
          ->select('purchase_detail.*',DB::raw("'$shop->name' as shop_code"),'product_supplier.fullname as supplier_name', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
          ->orderBy('purchase.created_at', 'DESC')
          ->get();
      }
      $purchaseList[] = $e;
    }
	}
    return view('.' . $view, [

      'purchaseList' => $purchaseList,
      'shops' => $shops,
      'purchaseDiscountTotalPrice' => $this->purchaseDiscountTotalPrice(0),
      'purchasePaymentAndDue' => $this->purchasePayment("monthly"),
      'supplierList' => ProductSupplier::get(),
    ]);
  }
  public function todaypurchaseList()
  {

    $type      = Input::get('type');
    $date_form = date('Y-m-d');
    $date_to   = date('Y-m-d');
    $id        = Input::get('id');
    $shops = ShopDetail::where('status', 1)->get();
    // search and vew reports condition
    $view = 'modules.purchase.purchase-list';

    if ($type == 'report') {
      $view = 'modules.reports.purchase.purchase-list-report';
    }
    $purchaseList = array();
    foreach ($shops as $shop) {
if (!preg_match('/[^A-Za-z0-9]/', $shop->code)){
      $dbname = 'pharmacy_' . $shop->code;
      \Config::set('database.connections.mysql.database', $dbname);
		\DB::purge('mysql');
        try {
		\DB::connection()->getPdo();
		} catch (\Exception $e) {
			continue;
		}
      // If date range search form summary sales
      if (!is_null($date_form) && !is_null($date_to)) {
        $e = PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
          ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
          ->leftjoin('product_supplier', 'product_supplier.id', 'product_add.supplier_id')
          ->select('purchase_detail.*',DB::raw("'$shop->name' as shop_code"),'product_supplier.fullname as supplier_name', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
          ->orderBy('purchase.created_at', 'DESC')
          ->whereBetween('purchased_date', [$date_form, $date_to])
          ->get();
      } elseif (!is_null($id)) {

        $e = PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
          ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
          ->leftjoin('product_supplier', 'product_supplier.id', 'product_add.supplier_id')
          ->select('purchase_detail.*',DB::raw("'$shop->name' as shop_code"),'product_supplier.fullname as supplier_name', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
          ->orderBy('purchase.created_at', 'DESC')
          ->where('purchase.supplier_id', $id)
          ->get();
      } else {
        $e = PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
          ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
          ->leftjoin('product_supplier', 'product_supplier.id', 'product_add.supplier_id')
          ->select('purchase_detail.*',DB::raw("'$shop->name' as shop_code"),'product_supplier.fullname as supplier_name', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
          ->orderBy('purchase.created_at', 'DESC')
          ->get();
      }
     $purchaseList[] = $e;

    }
	}
    return view('.' . $view, [

      'purchaseList' => $purchaseList,
      'shops' => $shops,
      'purchaseDiscountTotalPrice' => $this->purchaseDiscountTotalPrice(0),
      'purchasePaymentAndDue' => $this->purchasePayment("monthly"),
      'supplierList' => ProductSupplier::get(),
    ]);
  }


  // Purchase payment 
  public function purchasePayment($purchaseType)
  {

    if ($purchaseType == 'monthly') { // monthly or todays
      return DB::table('orders')
        ->first(array(
          DB::raw('SUM(orders.paid_amount) as totalVendorPaid'),
        ));
    } else {
      return DB::table('orders')
        ->first(array(
          DB::raw('SUM(orders.paid_amount) as totalVendorPaid'),
        ));
    }
  }
  // Purchase due
  public function purchaseDue($purchaseType)
  {

    if ($purchaseType == 'monthly') { // monthly or todays
      return DB::table('purchase_order')
       
        ->first(array(
          DB::raw('SUM(purchase_order.total_value) as vendorPaymentDue'),
        ));
    } else {
      return DB::table('purchase_order')
        ->where([
          
          ['purchase_order.created_at', date('Y-m-d')]
        ])
        ->first(array(
          DB::raw('SUM(purchase_order.total_value) as vendorPaymentDue'),
        ));
    }
  }

  // Todays purchase discount 

  public function todaysPurchaseDisocunt()
  {
    return DB::table('purchase')->where([
      ['purchase.shop_id', Session::get('shop_id')],
      ['purchase.purchased_date', date('Y-m-d')],
      ['purchase.approve_status', 1],
    ])
      ->first(array(
        DB::raw('SUM(purchase.sub_total) as purchaseSubTotal'),
        DB::raw('SUM(purchase.discount) as totalDiscount'),
        DB::raw('SUM(purchase.total_amount) as totalPayableAmount'),
      ));
  }

  // Purchase actual price and discount 
  public function purchaseDiscountTotalPrice($purchaseId)
  {

    if ($purchaseId == 0) { // list of all payment in current month
      return DB::table('purchase_order_item')
      ->first(array(
          DB::raw('SUM(purchase_order_item.price) as purchaseSubTotal'),
          DB::raw('SUM(purchase_order_item.discount) as totalDiscount'),
          DB::raw('SUM(purchase_order_item.amount) as totalPayableAmount'),
        ));
    } else { // specific purchase 

      return DB::table('purchase_order_item')
        ->first(array(
          DB::raw('SUM(purchase_order_item.discount) as totalDiscount'),
          DB::raw('SUM(purchase_order_item.amount) as totalPayableAmount'),
          DB::raw('SUM(purchase_order_item.amount) as totalPrePaid'),
          DB::raw('SUM(purchase_order_item.amount) as totalDue'),
        ));
    }
  }
  // Purchase payment
  public function purchasePaymentList()
  {

    // If date range search form summary sales
    return view('modules.purchase.purchase-payment', [

      'purchaseList' => Purchase::leftjoin('product_supplier', 'product_supplier.id', 'purchase.supplier_id')->select('product_supplier.fullname', 'purchase.*')->orderBy('purchase.created_at', 'DESC')
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.paid_status', 0],
          ['purchase.month', date('m')],
          ['purchase.year', date('Y')],
        ])
        ->get(),
    ]);
  }

  public function purchasePaymentConfirmation(Request $request)
  {

    // Purchase Payment
    $paymentTransactions = new PaymentTransactions();
    $paymentTransactions->shop_id           = Session::get('shop_id');
    $paymentTransactions->branch_id         = Session::get('branch_id');
    $paymentTransactions->transaction_type  = 1; // puchase payment
    $paymentTransactions->transaction_id    = $request->get('purchase_id');
    $paymentTransactions->payment_method    = $request->get('payment_method');
    $paymentTransactions->payment_date      = $request->get('payment_date');
    $paymentTransactions->total_amount      = $request->get('vendor_total_payable');
    $paymentTransactions->discount          = $request->get('discount');
    $paymentTransactions->paid_amount       = $request->get('vendor_paid_amount');
    $paymentTransactions->due_amount        = $request->get('vendor_due_amount');
    $paymentTransactions->bank_account      = $request->get('bank_account');
    $paymentTransactions->account_number    = $request->get('account_number');
    $paymentTransactions->created_by        = Session::get('user_id');
    $paymentTransactions->updated_by        = 0;
    $paymentTransactions->deleted_by        = 0;
    $paymentTransactions->save();

    // Due status
    if ($request->get('vendor_due_amount') != 0) {
      $paid_status = 0; // paid with due
      $is_due = 1; // Due
    } else {
      $paid_status = 1; // full paid
      $is_due = 0; // full paid

    }

    // Paid amount
    $prePaidAmount = Purchase::select('paid_amount')->where('purchase_id', $request->get('purchase_id'))->first();
    $totalPaid = $prePaidAmount->paid_amount + $request->get('vendor_paid_amount');

    // Update purchase table 
    Purchase::where('purchase_id', $request->get('purchase_id'))
      ->update([
        'paid_amount' => $totalPaid,
        'due_amount'  => $request->get('vendor_due_amount'),
        'paid_status' => $paid_status,
        'is_due' => $is_due,
      ]);


    return redirect('/accounts/purchase/payment')->with(['success' => "Your purchase payment has added successfully!",]);
  }
  public function purchaseApprovalPending()
  {
    $type      = Input::get('type');
    $view = 'modules.reports.purchase.purchase-approval-pending';
    return view('.' . $view, [

      'purchaseList' => PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
        ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
        ->select('purchase_detail.*', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
        ->orderBy('purchase.created_at', 'DESC')
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase_detail.status', 0],
        ])
        ->get(),
    ]);
  }

  public function todaysPurchase()
  {
    $type = Input::get('type');

    // search and vew reports condition
    $view = 'modules.purchase.todays-purchase';

    if ($type == 'report') {
      $view = 'modules.reports.purchase.todays-purchase-report';
    }

    return view('.' . $view, [

      'purchaseList' => PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
        ->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
        ->select('purchase_detail.*', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'purchase.purchase_id as purchaseId', 'purchase.discount as purchaseDiscount', 'purchase.due_amount as purchaseDue', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
        ->orderBy('purchase.created_at', 'DESC')
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.purchased_date', date('Y-m-d')],
          ['purchase_detail.status', 1],
        ])
        ->get(),
      'purchaseDiscountTotalPrice' => $this->todaysPurchaseDisocunt(),
      'purchasePayment' => $this->purchasePayment("todays"),
      'purchaseDue' => $this->purchaseDue("todays"),
    ]);
  }

  public function viewPurchaseInvoice($purchaseId)
  {

    return view('modules.purchase.view-purchase-receipt', [
      'purchaseList' => Purchase::leftjoin('purchase_detail', 'purchase.purchase_id', '=', 'purchase_detail.purchase_id')
        ->leftjoin('product_add', 'product_add.id', 'purchase_detail.product_id')
        ->select('purchase_detail.*', 'purchase.*', 'product_add.name as product_name')
        ->orderBy('purchase.created_at', 'DESC')
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase_detail.purchase_id', $purchaseId],
        ])
        ->get(),
      'purchaseInfo' => Purchase::where([
        ['shop_id', Session::get('shop_id')],
        ['purchase_id', $purchaseId],
      ])->first(),
      'paymentInfo' => Purchase::leftjoin('payment_transactions', 'purchase.purchase_id', 'payment_transactions.transaction_id')
        ->leftjoin('bank', 'bank.id', 'payment_transactions.bank_account')
        ->select('payment_transactions.*', 'bank.name as bank_name')
        ->where([
          ['payment_transactions.transaction_id', $purchaseId],
          ['payment_transactions.shop_id', Session::get('shop_id')],
        ])
        ->first(),
      'supplierDetail' => ProductSupplier::leftjoin('purchase', 'purchase.supplier_id', 'product_supplier.id')
        ->where([
          // ['product_supplier.shop_id',Session::get('shop_id')],
          ['purchase.purchase_id', $purchaseId],
        ])
        ->first(),
      'shopDetail' => ShopDetail::leftjoin('division', 'division.id', 'shop_info.division_id')
        ->leftjoin('district', 'district.id', 'shop_info.district_id')
        ->leftjoin('upazila', 'upazila.id', 'shop_info.upazila_id')
        ->leftjoin('union', 'union.id', 'shop_info.union_id')
        ->select('division.division_name', 'district.district_name', 'upazila.upazila_name', 'union.union_name', 'shop_info.*')
        ->where('shop_info.id', Session::get('shop_id'))
        ->first(),
      'bankList' => BankSetup::get(),
      'purchaseId' => $purchaseId,
      'totalAndDiscount' => $this->purchaseDiscountTotalPrice($purchaseId),
    ]);
  }

  public function lastPurchaseHistoryByProdct(Request $request)
  {

    $productId = $request->get('product_id');

    $purchaseDetails = Purchase::leftjoin('purchase_detail', 'purchase.purchase_id', '=', 'purchase_detail.purchase_id')
      ->leftjoin('product_add', 'product_add.id', 'purchase_detail.product_id')
      ->select('purchase_detail.*', 'purchase.*', 'product_add.name as product_name')
      ->orderBy('purchase.purchased_date', 'DESC')
      ->where([
        ['purchase.shop_id', Session::get('shop_id')],
        ['purchase_detail.product_id', $productId],
        ['purchase_detail.status', 1],
      ])
      ->limit(5)
      ->get();

    return view('modules.purchase.last-purchase-history-by-prodct', [
      'purchaseDetails' => $purchaseDetails
    ]);
  }

  public function approvePurchase($purchaseApprovedID, $purchase_id)
  {

    $purchase = PurchaseDetail::where('purchase_id', $purchase_id)
      ->where('id', $purchaseApprovedID)
      ->get();

    // Add current stock
    foreach ($purchase as $key => $purchaseVal) {


      $stockInfo = Stocks::where('product_id', $purchaseVal->product_id)
        ->where('shop_id', Session::get('shop_id'))
        ->first();

      $batchArray[] = array(
        'purchase_id'      => $purchaseVal->purchase_id,
        'batch_no'         => $purchaseVal->lot_no,
        'expiry_date'      => $purchaseVal->expiry_date,
        'quantity'         => $purchaseVal->purchased_qty,
        'unit_price'       => $purchaseVal->unit_price,
        'damage_status'    => 0, // default 0 for not damage and 1 for damaged
      );

      $batch = json_encode($batchArray);


      if (empty($stockInfo)) {

        // batch wise stock setup
        $newStocks = new Stocks();
        $newStocks->shop_id        = Session::get('shop_id');
        $newStocks->branch_id      = Session::get('branch_id');
        $newStocks->category_id    = $purchaseVal->category_id;
        $newStocks->product_id     = $purchaseVal->product_id;
        $newStocks->batch          = $batch;
        $newStocks->created_by     = Session::get('user_id');
        $newStocks->updated_by     = 0;
        $newStocks->deleted_by     = 0;
        $newStocks->save();
      } else {

        $resultBatch = json_decode($stockInfo->batch);


        $batchUpdate = array_merge($resultBatch, $batchArray);


        $stockInfo->shop_id        = Session::get('shop_id');
        $stockInfo->branch_id      = Session::get('branch_id');
        $stockInfo->category_id    = $purchaseVal->category_id;
        $stockInfo->product_id     = $purchaseVal->product_id;
        $stockInfo->batch          = json_encode($batchUpdate);
        $stockInfo->created_by     = Session::get('user_id');
        $stockInfo->updated_by     = 0;
        $stockInfo->deleted_by     = 0;
        $stockInfo->save();
      }

      ProductSetup::where('id', $purchaseVal->product_id)
        ->update(['total_tp' => $purchaseVal->unit_price]);

      // CURRENT
      $currentStockInfo = CurrentStock::where('product_id', $purchaseVal->product_id)
        ->where('shop_id', Session::get('shop_id'))
        ->first();

      // If no current stock
      if (empty($currentStockInfo)) {

        $stock = new CurrentStock();
        $stock->shop_id       = Session::get('shop_id');
        $stock->product_id    = $purchaseVal->product_id;
        $stock->quantity      = $purchaseVal->purchased_qty;
        $stock->expiry_date   = $purchaseVal->expiry_date;
        $stock->last_modified = date('Y-m-d h:i:s');
        $stock->created_by    = Session::get('user_id');
        $stock->updated_by    = 0;
        $stock->deleted_by    = 0;
        $stock->save();
      } else {


        // stock update
        $totalStock = $currentStockInfo->quantity + $purchaseVal->purchased_qty;

        CurrentStock::where('product_id', $purchaseVal->product_id)
          ->where('shop_id', Session::get('shop_id'))
          ->update([
            'quantity'      => $totalStock,
            'expiry_date'   => $purchaseVal->expiry_date,
            'last_modified' => date('Y-m-d h:i:s')
          ]);
      }
      // Purchase approved in purchase table
      Purchase::where('purchase_id', $purchase_id)
        ->update(['approve_status' => 1]);

      // Purchase approved  
      PurchaseDetail::where('id', $purchaseApprovedID)
        ->update(['status' => 1]);
    }

    $system_ip  = $_SERVER['REMOTE_ADDR'];
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $login_date = date('Y-m-d H:i:s');

    // log access
    $logAccess =  new LogPurchase();
    $logAccess->shop_id    = Session::get('shop_id');
    $logAccess->branch_id  = 1;
    $logAccess->user_id    = Session::get('user_id');
    $logAccess->email      = Session::get('email');
    $logAccess->system_ip  = $system_ip;
    $logAccess->login_date = $login_date;
    $logAccess->user_agent = $user_agent;
    $logAccess->access_details = "Purchase has approved with the purchase ID:" . $purchase_id;
    $logAccess->save();

    return redirect('/purchase/list')->with(['success' => "Your purchase has approved successfully!",]);
  }

  /**
   * Purchase alert
   */
  public function purchaseAlert()
  {

    $shopId = Session::get('shop_id');
    $type   = Input::get('type');
    $product_suppllier   = Input::get('product_suppllier');
    $product_category   = Input::get('product_category');

    $view = 'modules.purchase.purchase-alert';

    // reports summery searching 
    if ($type == 'report' || !empty($product_suppllier) || $product_category) {
      $view = 'modules.reports.purchase.purchase-alert';
    }

    // supplier search product
    if (!empty($product_suppllier)) {

      $currentStock = DB::select("SELECT p.name, p.strength, p.min_order_level, p.trade_price,p.supplier_id, p.sell_price, c.quantity, c.last_modified, pc.name as category_name FROM `product_add` p inner join currentstock c on p.id = c.product_id inner join product_category pc on pc.id=p.category WHERE c.shop_id = '$shopId' and  p.supplier_id = $product_suppllier and p.min_order_level > c.quantity");

      // product category search
    } elseif (!empty($product_category)) {

      $currentStock = DB::select("SELECT p.name, p.strength, p.min_order_level, p.trade_price, p.sell_price, c.quantity, c.last_modified, pc.name, pc.id as category_name FROM `product_add` p inner join currentstock c on p.id = c.product_id inner join product_category pc on pc.id=p.category WHERE c.shop_id = '$shopId' and  pc.id = $product_category  and p.min_order_level > c.quantity");
      // master ports
    } else {
      $currentStock = DB::select("SELECT p.name, p.strength, p.min_order_level, p.trade_price, p.sell_price, c.quantity, c.last_modified, pc.name as category_name FROM `product_add` p inner join currentstock c on p.id = c.product_id inner join product_category pc on pc.id=p.category WHERE c.shop_id = '$shopId' and p.min_order_level > c.quantity");
    }


    return view($view, [
      'currentStock' => $currentStock,
      'supplierList' => ProductSupplier::all(),
      'productCategory' => ProductCategory::all(),
    ]);
  }

  /**
   * Purchase and sales tracer
   * @return-tracer view
   */
  public function purchaseSalesTracerView()
  {
    return view('modules.purchase.tracer.index', [
      'supplierList' => ProductSupplier::all(),
    ]);
  }

  /**
   * Purchase and sales tracer
   * @return-tracer invoice
   */

  public function purchaseSalesTracerInvoice()
  {

    $searchType = Input::get('searchType');
    $searchId   = Input::get('searchId');

    if ($searchType == 'purchase') {
      $purchaseInfo = Purchase::where([
        ['shop_id', Session::get('shop_id')],
        ['purchase_id', $searchId],
      ])->first();
      if (!empty($purchaseInfo)) {
        return view('modules.purchase.tracer.purchase-invoice', [
          'purchaseList' => Purchase::leftjoin('purchase_detail', 'purchase.purchase_id', '=', 'purchase_detail.purchase_id')
            ->leftjoin('product_add', 'product_add.id', 'purchase_detail.product_id')
            ->select('purchase_detail.*', 'purchase.*', 'product_add.name as product_name')
            ->orderBy('purchase.created_at', 'DESC')
            ->where([
              ['purchase.shop_id', Session::get('shop_id')],
              ['purchase_detail.purchase_id', $searchId],
            ])
            ->get(),
          'purchaseInfo' => $purchaseInfo,
          'paymentInfo' => Purchase::leftjoin('payment_transactions', 'purchase.purchase_id', 'payment_transactions.transaction_id')
            ->leftjoin('bank', 'bank.id', 'payment_transactions.bank_account')
            ->select('payment_transactions.*', 'bank.name as bank_name')
            ->where([
              ['payment_transactions.transaction_id', $searchId],
              ['payment_transactions.shop_id', Session::get('shop_id')],
            ])
            ->first(),
          'supplierDetail' => ProductSupplier::leftjoin('purchase', 'purchase.supplier_id', 'product_supplier.id')
            ->where([
              ['product_supplier.shop_id', Session::get('shop_id')],
              ['purchase.purchase_id', $searchId],
            ])
            ->first(),
          'shopDetail' => ShopDetail::leftjoin('division', 'division.id', 'shop_info.division_id')
            ->leftjoin('district', 'district.id', 'shop_info.district_id')
            ->leftjoin('upazila', 'upazila.id', 'shop_info.upazila_id')
            ->leftjoin('union', 'union.id', 'shop_info.union_id')
            ->select('division.division_name', 'district.district_name', 'upazila.upazila_name', 'union.union_name', 'shop_info.*')
            ->where('shop_info.id', Session::get('shop_id'))
            ->first(),
          'purchaseId' => $searchId,
          'totalAndDiscount' => $this->purchaseDiscountTotalPrice($searchId),
        ]);
      } else {

        return "No result found!";
      }
    } else if ($searchType == 'sales') {

      $salesInfo = Sales::leftjoin('customer', 'sales.customer_id', '=', 'customer.id')
        ->select('customer.*', 'sales.*')
        ->where([
          ['sales.shop_id', Session::get('shop_id')],
          ['sales.sales_id', $searchId],
        ])
        ->first();

      if (!empty($salesInfo)) {
        return view('modules.purchase.tracer.sales-invoice', [
          'customer' => $salesInfo,
          'salesDetail' => Sales::leftjoin('sales_detail', 'sales.sales_id', '=', 'sales_detail.sales_id')
            ->leftjoin('product_add', 'product_add.id', 'sales_detail.product_id')
            ->select('sales.*', 'sales_detail.*', 'sales_detail.product_quantity as productQty', 'product_add.name as brandName', 'product_add.generic_name as genericName')
            ->where([
              ['sales.shop_id', Session::get('shop_id')],
              ['sales_detail.sales_id', $searchId],
            ])
            ->get(),

          'shopDetail' => ShopDetail::leftjoin('division', 'division.id', 'shop_info.division_id')
            ->leftjoin('district', 'district.id', 'shop_info.district_id')
            ->leftjoin('upazila', 'upazila.id', 'shop_info.upazila_id')
            ->leftjoin('union', 'union.id', 'shop_info.union_id')
            ->select('division.division_name', 'district.district_name', 'upazila.upazila_name', 'union.union_name', 'shop_info.*')
            ->where('shop_info.id', Session::get('shop_id'))
            ->first(),
        ]);
      } else {
        return "No result found!";
      }
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $invoiceNo = $request->get('invoice_no');

    //Purchase validation
    $validatedData = $request->validate([
      'purchase_id'         => 'required|unique:purchase|max:20',
      'supplier_id'         => 'required',
      'expiry_date.*'       => 'required',
      'manufacture_date.*'  => 'required',
    ]);


    $purchase = new Purchase();
    $purchased_date = explode('/', $request->get('purchased_date'));
    $invoice_date   = explode('/', $request->get('invoice_date'));

    $purchase->shop_id          = Session::get('shop_id');
    $purchase->purchase_id      = $request->get('purchase_id');
    $purchase->invoice_no       = $invoiceNo;
    $purchase->invoice_date     = $invoice_date[2] . '-' . $invoice_date[1] . '-' . $invoice_date[0];
    $purchase->challan_no       = $request->get('challan_no');
    $purchase->challan_date     = $request->get('challan_date');
    $purchase->supplier_id      = $request->get('supplier_id');
    $purchase->purchased_by     = $request->get('purchased_by');
    $purchase->purchased_date   = $purchased_date[2] . '-' . $purchased_date[1] . '-' . $purchased_date[0];
    $purchase->purchase_remarks = $request->get('purchase_remarks');
    $purchase->sub_total        = $request->get('sub_total');
    $purchase->total_amount     = $request->get('totalAmount');
    $purchase->discount         = $request->get('discount');
    $purchase->due_amount       = $request->get('totalAmount'); // initially total amount is same as due amount
    $purchase->paid_status      = 0; // 0= due, 1= paid
    $purchase->month            = date('m');
    $purchase->year             = date('Y');
    $purchase->created_by       = Session::get('user_id');
    $purchase->updated_by       = 0;
    $purchase->deleted_by       = 0;
    $purchase->save();


    // Product  detail
    $category_id      = $request->get('category_id');
    $product_id       = $request->get('product_id');
    $opening          = $request->get('opening');
    $lot_no           = $request->get('lot_no');
    $expiry_date      = $request->get('expiry_date');
    $manufacture_date = $request->get('manufacture_date');
    $purchased_qty    = $request->get('purchased_qty');
    $unit_price       = $request->get('unit_price');
    $vat              = $request->get('vat');
    $total_price      = $request->get('total_price');
    $comments         = $request->get('comments');



    foreach ($product_id as $key => $value) {

      $productID = !empty($product_id[$key]) ? $product_id[$key] : 0;


      $expDate = $expiry_date[$key];

      $exp_str = str_replace('/', '-', $expDate);
      $expiryDate =  date('Y-m-d', strtotime($exp_str));

      $manuDate        = $manufacture_date[$key];
      $manufac_str     = str_replace('/', '-', $manuDate);
      $manufactureDate = date('Y-m-d', strtotime($manufac_str));

      // Purchase Detail
      $purchaseDetails = new PurchaseDetail();
      $purchaseDetails->shop_id         = Session::get('shop_id');
      $purchaseDetails->purchase_id     = $request->get('purchase_id');
      $purchaseDetails->category_id     = 0;
      $purchaseDetails->product_id      = !empty($product_id[$key]) ? $product_id[$key] : 0;
      $purchaseDetails->opening         = !empty($opening[$key]) ? $opening[$key] : 0;
      $purchaseDetails->lot_no          = !empty($lot_no[$key]) ? $lot_no[$key] : 0;
      $purchaseDetails->expiry_date     = $expiryDate;
      $purchaseDetails->manufacture_date = $manufactureDate;
      $purchaseDetails->purchased_qty   = !empty($purchased_qty[$key]) ? $purchased_qty[$key] : 0;
      $purchaseDetails->unit_price      = !empty($unit_price[$key]) ? $unit_price[$key] : 0;
      $purchaseDetails->vat             = !empty($vat[$key]) ? $vat[$key] : 0;
      $purchaseDetails->total_price     = !empty($total_price[$key]) ? $total_price[$key] : 0;
      $purchaseDetails->comments        = !empty($comments[$key]) ? $comments[$key] : null;
      $purchaseDetails->created_by      = Session::get('user_id');
      $purchaseDetails->updated_by      = 0;
      $purchaseDetails->deleted_by      = 0;
      $purchaseDetails->save();

      $system_ip  = $_SERVER['REMOTE_ADDR'];
      $user_agent = $_SERVER['HTTP_USER_AGENT'];
      $login_date = date('Y-m-d H:i:s');

      // log access
      $logAccess =  new LogPurchase();
      $logAccess->shop_id    = Session::get('shop_id');
      $logAccess->branch_id  = 1;
      $logAccess->user_id    = Session::get('user_id');
      $logAccess->email      = Session::get('email');
      $logAccess->system_ip  = $system_ip;
      $logAccess->login_date = $login_date;
      $logAccess->user_agent = $user_agent;
      $logAccess->access_details = "New purchase added with the purchase ID" . $request->get('purchase_id');
      $logAccess->save();
    }

    return Session::get('shop_id') . date('YmdHis');
  }
}
