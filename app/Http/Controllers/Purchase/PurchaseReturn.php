<?php

namespace App\Http\Controllers\Purchase;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Modules\Purchase\Purchase;
use App\Model\Modules\Purchase\PurchasePayment;
use App\Model\Modules\Purchase\PurchaseDetail;
use App\Model\Modules\Purchase\PurchaseReturnModel;
use App\Model\Modules\Stock\CurrentStock;
use App\Model\Modules\Product\ProductSetup;
use App\Model\Modules\Product\ProductUnits;
use App\Model\Modules\Product\ProductCategory;
use App\Model\Modules\Product\ProductGroup;
use App\Model\Modules\Product\ProductManufacturer;
use App\Model\Modules\Product\ProductOrigin;
use App\Model\Modules\Product\ProductRack;
use App\Model\Modules\Product\ProductSupplier;
use App\Model\Modules\HRM\EmployeeRecords;
use App\Model\Modules\Settings\Bank as BankSetup;
use App\Model\Modules\Settings\ShopInfo as ShopDetail;
use Session;
use Input;
class PurchaseReturn extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.purchase.purchase-return.index',[
          'supplierList' => ProductSupplier::where('shop_id', Session::get('shop_id'))->get(),
        ]);
    }

    public function purchaseReturnInvoice($purchaseId)
    {      
      
      $view ='modules.purchase.purchase-return.purchase-return-invoice';

      $checkPurchase = Purchase::where([
            ['purchase.shop_id', Session::get('shop_id')],
            ['purchase.purchase_id', $purchaseId],
          ])
          ->first();
          
      if (!empty($checkPurchase)) {
        return view( $view, [
          'purchaseList' => Purchase::leftjoin('purchase_detail','purchase.purchase_id','=','purchase_detail.purchase_id')
            ->leftjoin('product_add','product_add.id','purchase_detail.product_id')
            ->select('purchase_detail.*','purchase.*','product_add.name as product_name')
            ->orderBy('purchase.created_at', 'DESC')
            ->where([
              ['purchase.shop_id', Session::get('shop_id')],
              ['purchase_detail.purchase_id', $purchaseId],
            ])
            ->get(),
          'purchaseInfo' => $checkPurchase,
          'purchaseId' => $checkPurchase->purchase_id,
          'paymentInfo' => Purchase::leftjoin('payment_transactions','purchase.purchase_id','payment_transactions.transaction_id')
            ->leftjoin('bank','bank.id','payment_transactions.bank_account')
            ->select('payment_transactions.*','bank.name as bank_name')
            ->where([
              ['payment_transactions.transaction_id', $purchaseId],
            ])  
            ->first(),   
          'supplierDetail' => ProductSupplier::leftjoin('purchase','purchase.supplier_id','product_supplier.id')
            ->where([
              ['product_supplier.shop_id',Session::get('shop_id')],
              ['purchase.purchase_id', $purchaseId],
            ])  
            ->first(),
          'shopDetail' => ShopDetail::leftjoin('division','division.id','shop_info.division_id')
            ->leftjoin('district','district.id','shop_info.district_id')
            ->leftjoin('upazila','upazila.id','shop_info.upazila_id')
            ->leftjoin('union','union.id','shop_info.union_id')
            ->select('division.division_name','district.district_name','upazila.upazila_name','union.union_name','shop_info.*')
            ->where('shop_info.id', Session::get('shop_id'))
            ->first(),  
        ]);
      } else {
        return "<h5 style='color:red;text-align:center;'> No result found by this purchase/invoice no.</h5>";
      }   
      
    }

    public function searchPurchaseInvoice()
    {      
      
      $type       = Input::get('type');
      $purchaseId = Input::get('purchase_id');

      if($type == 'report') {
        $view ='modules.purchase.purchase-return.purchase-return-invoice-search-result';
      }      
    
      $checkPurchase = Purchase::where([
            ['purchase.shop_id', Session::get('shop_id')],
            ['purchase.purchase_id', $purchaseId],
          ])
          ->first();
      if (!empty($checkPurchase)) {
        return view( $view, [
          'purchaseList' => Purchase::leftjoin('purchase_detail','purchase.purchase_id','=','purchase_detail.purchase_id')
            ->leftjoin('product_add','product_add.id','purchase_detail.product_id')
            ->select('purchase_detail.*','purchase.*','product_add.name as product_name')
            ->orderBy('purchase.created_at', 'DESC')
            ->where([
              ['purchase.shop_id', Session::get('shop_id')],
              ['purchase_detail.purchase_id', $purchaseId],
            ])
            ->get(),
          'purchaseInfo' => $checkPurchase,
          'purchaseId' => $checkPurchase->purchase_id,
          'paymentInfo' => Purchase::leftjoin('payment_transactions','purchase.purchase_id','payment_transactions.transaction_id')
            ->leftjoin('bank','bank.id','payment_transactions.bank_account')
            ->select('payment_transactions.*','bank.name as bank_name')
            ->where([
              ['payment_transactions.transaction_id', $purchaseId],
            ])  
            ->first(),   
          'supplierDetail' => ProductSupplier::leftjoin('purchase','purchase.supplier_id','product_supplier.id')
            ->where([
              // ['product_supplier.shop_id',Session::get('shop_id')],
              ['purchase.purchase_id', $purchaseId],
            ])  
            ->first(),
          'shopDetail' => ShopDetail::leftjoin('division','division.id','shop_info.division_id')
            ->leftjoin('district','district.id','shop_info.district_id')
            ->leftjoin('upazila','upazila.id','shop_info.upazila_id')
            ->leftjoin('union','union.id','shop_info.union_id')
            ->select('division.division_name','district.district_name','upazila.upazila_name','union.union_name','shop_info.*')
            ->where('shop_info.id', Session::get('shop_id'))
            ->first(),  
        ]);
      } else {
        return "<h5 style='color:red;text-align:center;'> No result found by this purchase/invoice no.</h5>";
      }   
      
    }

    public function purchaseListReturn()
    {
        // Dynamic report from master reports development
        $type   = Input::get('type');
        $view   = 'modules.purchase.purchase-return.purchase-return-list';
        $shopID = Session::get('shop_id');

        // purchase list 
        if($type == 'report') {
        	$view = 'modules.reports.purchase.purchase-list';
        }
	
//	    $purchaseReturnData = PurchaseReturnModel::rightJoin('purchase_detail', 'purchase_detail.purchase_id', '=', 'purchase_return.purchase_id')
//		    ->select('purchase_return.*', 'purchase_return.id as returnID', 'purchase_detail.*')
//		    ->where('purchase_return.shop_id', $shopID)
////		    ->whereBetween('purchase_return.created_at',[Date('Y-m-').'01 00:00:00', Date('Y-m-').'31 23:59:59'])
//		    ->where('purchase_return.approve_status', 0)
//		    ->get();
	
	    $returnReqestData = PurchaseReturnModel::where('purchase_return.shop_id', $shopID)
	        ->where('purchase_return.approve_status', 1)
	        ->get();
	    
	    $returnRecords = [];
	    
	    foreach ($returnReqestData as $index => $data) {
	     
	    	$info = [];
	    	$info['purchase_id']    = $data['purchase_id'];
	    	$info['product_id']     = $data['product_id'];
	    	$info['lot_no']         = $data['lot_no'];
		    $info['return_qty']     = $data['return_qty'];
		    $info['approve_status'] = $data['approve_status'];
		    $info['returnID']       = $data['id']; // Return table id  as a return id
		    
		    $productDetails = PurchaseDetail::where('shop_id', $shopID)
			    ->where('purchase_id', $data['purchase_id'])
			    ->where('product_id', $data['product_id'])
			    ->where('lot_no', $data['lot_no'])
			    ->first();
		    
		    $info['expiry_date']    = $productDetails->expiry_date ?? "";
		    $info['purchased_qty']  = $productDetails->purchased_qty ?? 0;
		    $returnRecords[]        = $info;
	    }
	    
        return view('.'.$view,[
            'purchaseReturn' => $returnRecords,
        ]);
    }

     public function lisOfPurchaseReturenDateSearch(Request $request)
    {
        // Dynamic report from master reports development
        $dateFrom = $request->get('dateFrom');
        $dateTo   = $request->get('dateTo');
	    $shopID   = Session::get('shop_id');
        
        $view = 'modules.purchase.purchase-return.purchase-return-list-search';
        
//        $purchaseReturnData = PurchaseReturnModel::leftJoin('purchase_detail', 'purchase_return.product_id', '=', 'purchase_detail.product_id')
//              ->select('purchase_return.*', 'purchase_return.id as returnID', 'purchase_detail.*')
//              ->where('purchase_return.shop_id', Session::get('shop_id'))
//              ->whereBetween('purchase_return.created_at',[$dateFrom.' 00:00:00',$dateTo.' 23:59:59'])
//              ->where('purchase_return.approve_status', 0)
//              ->get();
     
	    $returnReqestData = PurchaseReturnModel::where('purchase_return.shop_id', $shopID)
		    ->whereBetween('purchase_return.created_at',[$dateFrom.' 00:00:00',$dateTo.' 23:59:59'])
		    ->where('purchase_return.approve_status', 1)
		    ->get();
	
	    $returnRecords = [];
	
	    foreach ($returnReqestData as $index => $data) {
		
		    $info = [];
		    $info['purchase_id']    = $data['purchase_id'];
		    $info['product_id']     = $data['product_id'];
		    $info['lot_no']         = $data['lot_no'];
		    $info['return_qty']     = $data['return_qty'];
		    $info['approve_status'] = $data['approve_status'];
		    $info['returnID']       = $data['id']; // Return table id  as a return id
		
		    $productDetails = PurchaseDetail::where('shop_id', $shopID)
			    ->where('purchase_id', $data['purchase_id'])
			    ->where('product_id', $data['product_id'])
			    ->where('lot_no', $data['lot_no'])
			    ->first();
		
		    $info['expiry_date']    = $productDetails->expiry_date ?? "";
		    $info['purchased_qty']  = $productDetails->purchased_qty ?? 0;
		    $returnRecords[]        = $info;
	    }
	
	    return view($view,[
		    'purchaseReturn' => $returnRecords,
	    ]);
    }

    // Result will display in modal for return confirmation
    public function purchaseReturnModalDisplay()
    {
      // Dynamic report from master reports development
      $purchaseId = Input::get('purchaseId');      
      
      $purchaseReturn = Purchase::leftjoin('product_supplier','product_supplier.id','purchase.supplier_id')
      ->select('purchase.*','product_supplier.fullname')
      ->where([
        ['purchase.shop_id', Session::get('shop_id')],
        ['purchase.purchase_id', $purchaseId],
        ])          
      ->get();

      return view('modules.purchase.purchase-return.modal-view-purchase-return',[
        'purchaseReturn' => $purchaseReturn,
      ]);
    }

    public function confirmPurchaseReturn($returnID, $internalCall=0)
    {
	    $returnData = PurchaseReturnModel::findOrFail($returnID);
	    
	    if (empty($returnData) && $internalCall) {
	    	return 400;
	    } else if (empty($returnData)) {
		    return redirect('list/of/purchase/return')->with(['error' => "Sorry! We cannot update your purchase return.",]);
	    }
	    
	    // Update current stock
	
	    $stock = CurrentStock::where('product_id', $returnData->product_id)
		    ->where('shop_id', Session::get('shop_id'))
		    ->first();
	    
	    if (!empty($stock)) {
		
		    $stockUpdate = CurrentStock::where('product_id', $returnData->product_id)
			    ->where('shop_id', Session::get('shop_id'))
			    ->update([
				    'quantity' => $stock->quantity - $returnData->return_qty
			    ]);
	    }
		
	    #Product Details
		$productDetails = PurchaseDetail::where('purchase_id', $returnData->purchase_id)
			->where('product_id', $returnData->product_id)
			->where('lot_no', $returnData->lot_no)
			->where('shop_id', Session::get('shop_id'))
			->first();
      
		$productUnitPrice = $productDetails->unit_price ?? 0;
		$changeAmount = $productUnitPrice * $returnData->return_qty;
		
		if (!empty($productDetails)) {
			
			$purchasedQty = $productDetails->purchased_qty - $returnData->return_qty;
			$totalPurchased  = $productDetails->total_price - $changeAmount;
			
			if ($purchasedQty == 0 || $totalPurchased == 0) {
				// If full purchase product returned then destroy
				$productUpdate = PurchaseDetail::where('purchase_id', $returnData->purchase_id)
					->where('product_id', $returnData->product_id)
					->where('shop_id', Session::get('shop_id'))
					->delete();
				
			} else {
				
				$productUpdate = PurchaseDetail::where('purchase_id', $returnData->purchase_id)
					->where('product_id', $returnData->product_id)
					->where('lot_no', $returnData->lot_no)
					->where('shop_id', Session::get('shop_id'))
					->update([
						'purchased_qty' => $productDetails->purchased_qty - $returnData->return_qty,
						'total_price'   => $productDetails->total_price - $changeAmount,
					]);
			}
			
		}
		#update into purchase table
	    $purchaseInfo = Purchase::where('shop_id', Session::get('shop_id'))
		    ->where('purchase_id', $returnData->purchase_id)->first();
	    
	    if (!empty($purchaseInfo)) {
	    	$totalAmountPurchased =  $purchaseInfo->total_amount - $changeAmount;
	    	
	    	if ($totalAmountPurchased == 0) {
	    		// If all product returned then destroy record
			    $updatePurchase = Purchase::where('shop_id', Session::get('shop_id'))
				    ->where('purchase_id', $returnData->purchase_id)
				    ->delete();
		    } else {
			
			    $updatePurchase = Purchase::where('shop_id', Session::get('shop_id'))
				    ->where('purchase_id', $returnData->purchase_id)
				    ->update([
					    'total_amount' => $totalAmountPurchased,
					    'paid_amount'  => abs($purchaseInfo->paid_amount - $changeAmount),
					    'due_amount'   => abs($purchaseInfo->due_amount  - $changeAmount),
					    'paid_status'  => 1,
					    'is_due'       => (($purchaseInfo->due_amount  - $changeAmount) > 0) ? 1: 0
				    ]);
		    }
	    }
	    
	    PurchaseReturnModel::where('id', $returnID)
	    ->where('shop_id', Session::get('shop_id'))
	    ->update([
		    'approve_status' => 1
	    ]);
	    
	    return 200;
	    
        // return redirect('list/of/purchase/return')->with(['success' => "Your purchase return record has updated successfully!",]);
	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purchaseId = $request->get('purchase_id');
        $productIds = $request->get('product_id');
        $returnQty  = $request->get('return_qty');
        $batchNo    = $request->get('batch_no');
        
		// Store into purchase return request
        foreach ($productIds as $index => $productID) {
			
        	if( $returnQty[$index] > 0)  {
		
		        $purchase = new PurchaseReturnModel();
		        $purchase->shop_id     = Session::get('shop_id');
		        $purchase->branch_id   = Session::get('branch_id');
		        $purchase->purchase_id = $purchaseId;
		        $purchase->product_id  = $productID;
		        $purchase->lot_no      = $batchNo[$index];
		        $purchase->return_qty  = $returnQty[$index];
		        $purchase->approve_status = 0;
		        $purchase->created_by     = Session::get('user_id');
		        $purchase->updated_by     = 0;
		        $purchase->deleted_by     = 0;
		        $purchase->save();
		        
		        // After purchase make sure auto approval
		        $returnID = $purchase->id;
		        
		        $this->confirmPurchaseReturn($returnID);
		        
	        }
        }

        return redirect('purchase/return')->with(['success' => "Your purchase return record has added successfully!",]);
  
    }

    public function show($value='')
    {
      # code...
    }
    
}
