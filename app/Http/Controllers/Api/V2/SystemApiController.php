<?php
namespace App\Http\Controllers\Api\V2;
use App\Models\Shop;
use App\Models\User;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ProductStock;
use App\Mail\InvoiceEmailManager;
use App\Http\Controllers\Controller;
use App\Http\Resources\POSProductsCollection;
use App\Http\Resources\POSProductsCollectionTest;
use App\Http\Resources\LoginInfoCollection;
use App\Http\Resources\AllIncomeCollection;
use App\Http\Resources\AllExpenceCollecton;
use App\Http\Resources\RecentSalesCollection;
use Illuminate\Http\Request;
use App\Model\Modules\Purchase\PurchaseDetail;
use App\Http\Controllers\Purchase\PurchaseManagement;
use App\Http\Controllers\Dashboard\Dashboard;
use App\Models\Purchase_order;
use App\Models\Purchase_order_item;
use Auth;
use DB;
use Mail;
use Carbon\Carbon;

class SystemApiController extends Controller
{

   // Constructor for all methods  
    public function __construct(){
      $this->yesterday = date('Y-m-d',strtotime("-1 days"));
      $this->today     = Carbon::today();
      $this->month     = date('m');
      $this->year      = date('Y');
    }

	public function userLoginInfo(Request $request)
	{
		
		$username     = $request->get('username');
		$password  = $request->get('password');
		$code  = $request->get('code');

		
		$validatedData = $request->validate([
			'username'  => 'required|max:55',
			'password'  => 'required',
		]);
       $user_info = User::where('users.name',$username)->first();
	   $shopInfo = Shop::where('shops.user_id',$user_info->id)->first();
       
	
		
		//        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password')])){
		if (Auth::attempt(['name' => $username, 'password' => $password])) {

			$access_token = password_hash(rand(10, 100), PASSWORD_BCRYPT);

			User::where('id', Auth::user()->id)->update(['access_token' => $access_token]);

			$userInfo   = User::where('id', Auth::user()->id)->select('id', 'name', 'email')->first();
		
			$listinfo = [];

			$listinfo[0] = array("user"=>$userInfo, "shop" => $shopInfo, "token" => $access_token);

			$listinfo = array_reverse($listinfo);
			

			return new LoginInfoCollection($listinfo);

			return response()->json([
				'status'    =>  200,
				'token'     => $access_token,
				'message'   => "Login successful",
				'data'      => [
					'userInfo' => $userInfo,
				
					'shopInfo' => $shopInfo
				]
			], 200);
		}

		# If not authorized to login
		return response()->json([
			'status'    =>  401,
			'message'   => "Wrong credentials"
		], 401);
	}

	public function getProducts(Request $request)
	{
		
		$user_id  = $request->get('user_id');
		$validatedData = $request->validate([
			'user_id'  => 'required',
		]);

		$listtest = [];

		$customer = User::where('user_type','customer')
		->select('id', 'name', 'email')->get();
       if(!empty($user_id )){
		$get_products = Product::
		leftjoin('categories','products.category_id','categories.id')
	   ->leftjoin('brands','products.brand_id','brands.id')
	   ->leftjoin('shops','products.user_id','shops.user_id')
	   ->leftjoin('product_stocks','products.id','product_stocks.product_id')
	   ->select('categories.id as catid','categories.name as category_name','brands.name as brand_name','shops.id as shop_id','product_stocks.id as stock_id','product_stocks.batch_no', 'product_stocks.price','product_stocks.variant','product_stocks.qty','products.*')
	   ->OrderBy('products.name')
	   ->where('product_stocks.qty','>',0)
	   ->get();
	   }
		$listtest[0] = array('customers' => $customer, 'get_products' => $get_products);
		$listtest = array_reverse($listtest);
		return new POSProductsCollection($listtest);
	}

	public function getProductsTest(Request $request)
	{
		
		$user_id  = $request->get('user_id');
		$validatedData = $request->validate([
			'user_id'  => 'required',
		]);

		$listtest = [];

		$customer = User::where('user_type','customer')
		->select('id', 'name', 'email')->get();
       if(!empty($user_id )){
		$get_products = Product::
		leftjoin('categories','products.category_id','categories.id')
	   ->leftjoin('brands','products.brand_id','brands.id')
	   ->leftjoin('shops','products.user_id','shops.user_id')
	   ->select('categories.id as catid','categories.name as category_name','brands.name as brand_name','shops.id as shop_id','products.*')
	   ->get();
	   }
		$listtest[0] = array('customers' => $customer, 'get_products' => $get_products);
		$listtest = array_reverse($listtest);
		return new POSProductsCollectionTest($listtest);
	}

	public function createSales(Request $request){

		$addressArray = array(
			'name'      => "Mr. Customer",
			'email'     =>  "customer@example.com",
			'address'   => "Mirpur",
			'country'   => "Bangladesh",
			'city'      =>  "Dhaka",
			'postal_code'  => "1216",
			'phone'    => "01821000000",
		  );
		  $address = json_encode($addressArray);
		 
            $order = new Order;
            if ($request->user_id == null) {
                $order->guest_id    = mt_rand(100000, 999999);
            }
            else {
                $order->user_id = $request->user_id;
            }
			$order->name= $request->customer_name;
            $order->phone= $request->customer_mobile_number;
            $order->shipping_address = $address;
            $order->payment_type = "cash_on_delivery";
            $order->delivery_viewed = '0';
            $order->payment_status_viewed = '0';
			$order->code = date('Ymd-His') . rand(10, 99);
            $order->date = strtotime(date('Y-m-d'));
            $order->order_from = "App";
			$order->paid_amount      = $request->get('paid_amount');
			$order->due_amount       = $request->get('due_amount');
            if($order->save()){
                $subtotal = 0;
                $tax = 0;

				$productdetails = json_decode($request->get('productdetails'));
                 
                foreach ($productdetails as $key => $cartItem){
                    
					$cartItems = (array)$cartItem;
					
					if($cartItems['strength']!=""){
						$stock = ProductStock::where([
							'product_id' => $cartItems['product_id'],'variant' => $cartItems['strength']
						])->first();
						
					}else{
						$stock = ProductStock::where([
							'id' => $cartItems['stock_id']
						])->first();
						
					}
					
					if($stock){
						$stock->qty -= $cartItem->quantity;
					    $stock->save();
					}
					
					$product = $stock->product;
                    $product_variation = $stock->variant;
                    $subtotal += $cartItem->unit_price*$cartItem->quantity;
					
                    $order_detail = new OrderDetail;
                    $order_detail->order_id  =$order->id;
                    $order_detail->seller_id = $product->user_id;
                    $order_detail->product_id = $product->id;
                    $order_detail->payment_status = $request->payment_type != 'cash_on_delivery' ? 'Paid' : 'Unpaid';
                    $order_detail->variation = $cartItem->strength;
                    $order_detail->unit_price = $cartItem->unit_price;
                    $order_detail->price = $cartItem->unit_price * $cartItem->quantity;
                    $order_detail->tax = 0.00;
                    $order_detail->quantity = $cartItem->quantity;
                    $order_detail->shipping_type = null;
                    $order_detail->shipping_cost = 0.00;
                    $order_detail->save();
                    $product->num_of_sale ++;
                    $product->save();
                }

                $order->grand_total = $subtotal + $tax +$request->shipping_cost;

                if(!empty($request->discount_amount)){
                    $order->grand_total -= $request->discount_amount;
                    $order->discount_amount = $request->discount_amount;
                }else{
					$order->grand_total = $order->grand_total;
				}

				if($request->paid_amount > 0 ){
					if($order->grand_total == $request->paid_amount){
						$order->payment_status = "Paid";
					}else{
						$order->payment_status = "Partial";
					}
				}else{
					$order->payment_status = "Due";
				}

                $order->seller_id = $product->user_id;
                $order->save();

                $array['view'] = 'emails.invoice';
                $array['subject'] = 'Your order has been placed - '.$order->code;
                $array['from'] = env('MAIL_USERNAME');
                $array['order'] = $order;

                $admin_products = array();
                $seller_products = array();

                foreach ($order->orderDetails as $key => $orderDetail){
                    if($orderDetail->product->added_by == 'admin'){
                        array_push($admin_products, $orderDetail->product->id);
                    }
                    else{
                        $product_ids = array();
                        if(array_key_exists($orderDetail->product->user_id, $seller_products)){
                            $product_ids = $seller_products[$orderDetail->product->user_id];
                        }
                        array_push($product_ids, $orderDetail->product->id);
                        $seller_products[$orderDetail->product->user_id] = $product_ids;
                    }
                }

                foreach($seller_products as $key => $seller_product){
                    try {
                        Mail::to(User::find($key)->email)->queue(new InvoiceEmailManager($array));
                    } catch (\Exception $e) {

                    }
                }

                //sends email to customer with the invoice pdf attached
                if(env('MAIL_USERNAME') != null){
                    try {
                        Mail::to($request->session()->get('pos.shipping_info')['email'])->queue(new InvoiceEmailManager($array));
                        Mail::to(User::where('user_type', 'admin')->first()->email)->queue(new InvoiceEmailManager($array));
                    } catch (\Exception $e) {

                    }
                }

				return response()->json([
					'status'    => 200,
					'message'   => "Your sales record has added successfully!",
				]);
            }
          
			return response()->json([
				'status' => 422,
				'message' => "We cannot process your data due to data quality issues!",
			]);
        
    }

	/**
	 * @return \Illuminate\Http\JsonResponse
	 */

	public function getPurchaseList(Request $request)
	{
        // $dateFrom = !empty($request->get('dateFrom')) ? date('Y-m-d',strtotime($request->get('dateFrom'))) : date('Y-m-01');
		// $dateTo   = !empty($request->get('dateTo')) ? date('Y-m-d',strtotime($request->get('dateTo'))) : date('Y-m-t');

		$from = date('Y-m-d',strtotime($request->dateFrom));
		$to = date('Y-m-d',strtotime($request->dateTo. ' +1 day'));

		DB::enableQueryLog();
		 $purchase_list = Purchase_order::whereBetween('purchase_order.created_at', [$from, $to])->get();
            $lists = [];
             $query = DB::getQueryLog();
             $query = end($query);
             
			 foreach ($purchase_list as $purchase_item) {

				$purchases = Purchase_order_item::where('purchase_order_item.po_id',$purchase_item->id)
				->join('products','purchase_order_item.product_id','products.id')
				->select('purchase_order_item.*','products.name as product_name')->get();

				foreach($purchases as $purchase )
				{
					$lists[$purchase['id']] =array(
						"id"=> $purchase_item->purchase_no,
						"shop_id"=> 1,
						"purchase_id"=> $purchase_item->purchase_no,
						"category_id"=> 0,
						"product_id"=> $purchase->product_id,
						"opening"=> 3,
						"purchased_qty"=> $purchase->qty,
						"unit_price"=> $purchase->price,
						"vat"=> 0,
						"total_price"=> $purchase->amount,
						"lot_no"=> "3456",
						"expiry_date"=> $purchase->expiry_date,
						"manufacture_date"=> "2022-02-22",
						"comments"=> null,
						"status"=> $purchase->status,
						"created_by"=> $purchase->created_by,
						"updated_by"=> 0,
						"deleted_by"=> 0,
						"created_at"=> date("Y-m-d", strtotime($purchase->created_at)),
						"updated_at"=> date("Y-m-d", strtotime($purchase->updated_at)),
						"purchaseApprovedID"=> $purchase->approvedStatus,
						"invoice_no"=> "123",
						"invoice_date"=> date("Y-m-d", strtotime($purchase->created_at)),
						"challan_no"=> null,
						"challan_date"=> null,
						"supplier_id"=> $purchase->supplier_id,
						"purchased_by"=> $purchase->created_by,
						"purchased_date"=> date("Y-m-d", strtotime($purchase->created_at)), 
						"purchase_remarks"=> $purchase->remarks,
						"month"=> 3,
						"year"=> 2022,
						"sub_total"=> 200,
						"discount"=> $purchase->discount,
						"total_amount"=>$purchase->total_value,
						"paid_amount"=> 0,
						"due_amount"=> 300,
						"paid_status"=> 0,
						"is_due"=> 0,
						"approve_status"=> 0,
						"product_name"=> $purchase->product_name,
						"approvedStatus"=> 0
					);
				}
                  
			 }

			$lists = array_reverse($lists);
		if (!empty($lists) && count($lists) > 0) {
			return response()->json([
				'status' => 200,
				'purchase_list' => $lists,
         'query'=>$query
			], 200);
		}

		return response()->json([
			'status' => 400,
			'message' => "No record found!",
		], 400);
	}

	public function getSupplier()
	{
		$suppliers = Supplier::all();
	    $lists = [];
	foreach ($suppliers as $supplier) {
		$lists[$supplier['id']]=  array(
			"id"=> $supplier->id,
			"shop_id"=> 1,
			"fullname"=> $supplier->name,
			"company_name"=> $supplier->name,
			"address"=> $supplier->address,
			"mobile"=> $supplier->phone,
			"email"=> $supplier->email,
			"status"=> $supplier->ststus,
			"created_by"=> 0,
			"updated_by"=> 0,
			"deleted_by"=> 0,
			"created_at"=> date("Y-m-d", strtotime($supplier->created_at)),
			"updated_at"=> date("Y-m-d", strtotime($supplier->updated_at)),

		 ); 
	}

	$lists = array_reverse($lists);
			return response()->json([
				'status' => 200,
				'supplier_list' => $lists
			], 200);
		

		return response()->json([
			'status' => 400,
			'message' => "No record found!",
		], 400);
	}


	public function getSupplierWiseProduct(Request $request)
	{
		$supplier_id = $request->get('supplier_id');

		$products = Product::where('supplier_id',$supplier_id)->paginate(10);
		
		$lists = [];
	
			foreach ($products as $product) {

			$lists[$product["id"]] = array(
				"product_id"=> $product->id,
				"product_name"=> $product->name,
				"generic_name"=> $product->brand->name,
				"dosageName"=> $product->name,
				"is_supplier"=> 1,
				"strength"=> $product->unit,
				"min_order_level"=> $product->min_qty,
				"trade_price"=> $product->purchase_price,
				"supplier_id"=> $product->supplier_id,
				"sell_price"=> $product->unit_price,
				"quantity"=> $product->current_stock,
				"last_modified"=> date("Y-m-d", strtotime($product->updated_at)),
				"category_name"=> $product->category->name,
				"available_stock"=> $product->current_stock

			);
		}

		$lists = array_reverse($lists);
		return response()->json([
			'status' => 200,
			'sql'=> "gfhfgh",
			'currentStock' => $lists
		], 200);
	}

	public function purchaseStore(Request $request)
    {
         
        if (!empty($request->purchase_id)) {
            $purchase = new Purchase_order();
            $purchase->purchase_no = $request->purchase_id;
            $purchase->supplier_id = $request->supplier_id;
            $purchase->date = date('Y-m-d');
            $purchase->total_value =  $request->totalAmount;
            $purchase->created_by = $request->purchased_by;
            $purchase->purchase_by = 'from_app';
            $purchase->discount = $request->discount;
            $purchase->save();

			$productdetails = json_decode($request->get('productdetails'));
			
                foreach ($productdetails as $key => $product) {
					$exp_date = $product->expire_date;
					$expdate = str_replace('/','-',$exp_date);
                    $item = new Purchase_order_item();
                    $item->po_id = $purchase->id;
                    $item->product_id = $product->product_id;
                    $item->qty = $product->quantity;
					$item->batch_no = $product->batch;
                    $item->expiry_date = date('Y-m-d',strtotime($expdate));
                    $item->desc = $product->product_name;
                    $item->price = $product->unit_price;
                    $item->amount = $product->unit_price* $product->quantity;
                    $item->save();

                        $p = Product::find($product->product_id);
                        $p->increment('current_stock', $product->quantity);
						$p->purchase_price = $product->unit_price;
						$p->save();
                }
				return response()->json([
					'status'    => 200,
					'message'   => "Thanks! Your purchase record has added successfully!",
				]);
            
			return response()->json([
				'status' => 422,
				'message' => "We cannot process your data due to data quality issues!",
			]);
        }
    }


public function getDashboardData(Request $request)
	{
		
		$start_date = !empty($request->get('start_date')) ? date('Y-m-d',strtotime($request->get('start_date'))) : date('Y-m-01');
		$end_date = !empty($request->get('end_date')) ? date('Y-m-d',strtotime($request->get('end_date'))) : date('Y-m-t');
		$purchaseObject = new PurchaseManagement;
        $dashboardCtrl = new Dashboard;
        $shopId = $request->shop_id;
        // Expiry calculation
		$todate = date('Y-m-d');
		$totalExpired = Purchase_order_item::
		leftjoin('products','purchase_order_item.product_id','products.id')
		->select('products.name','products.unit','purchase_order_item.*')
		->where('expiry_date','<=',$todate)->count();
    

			$purchaseDetail = Purchase_order_item::leftjoin('purchase_order','purchase_order_item.po_id','purchase_order.id')
		   ->select('purchase_order.id as month','purchase_order_item.amount as total_price')
		   ->whereBetween('purchase_order.created_at',[$start_date, $end_date])
		   ->get();

		   $allIncome = Order::leftjoin('order_details','orders.id','order_details.order_id')
		   ->select('orders.name','orders.payment_type')
		   ->get();

		   $allIexpence = Purchase_order::leftjoin('purchase_order_item','purchase_order.id','purchase_order_item.po_id')
		   ->select('purchase_order.purchase_no','purchase_order.remarks')
		   ->get();

		   $recent_salse = Order::leftjoin('users', 'users.id', 'orders.user_id')
        ->select('orders.*', 'users.name as customerName')
        ->orderBy('orders.created_at', 'DESC')
        ->get(100);
      
		$dashboardData = [

			// Today's purchase, sales, collections and dues
			'todaysPurchase' => $dashboardCtrl->purchase('adminTodays'),
			'todaysPurchaseDiscount' => $dashboardCtrl->purchaseDiscount('adminTodays'), 
			'todaysSales' => $dashboardCtrl->sales('adminTodays'), 
			'todaysCollection' => $dashboardCtrl->collectionAmount('adminTodays'), 
			'todaysDue' => array($dashboardCtrl->dueAmount('adminTodays')),
			
			// Stockout  
			'stockOut' => DB::table('product_stocks')
			  ->select(DB::raw('count(product_stocks.product_id) as stock_out'))
			  ->where([
					['product_stocks.qty','<=', 0]
				  ]
			  )
			  ->first(), 
  
			// Expired products
			
			'expiredProducts' => $totalExpired,
			
			  
			// Monthly purchase and sales comparison  
			'monthlyPurchase' =>$purchaseDetail,
  
			'purchaseDiscountTotalPrice' => $purchaseObject->purchaseDiscountTotalPrice(0),   
			'purchasePayment' => $purchaseObject->purchasePayment("monthly"),   
			'purchaseDue' => $purchaseObject->purchaseDue("monthly"),   
  
			// Monthly sales summary - chart report  
			'monthlySales' => DB::table('order_details')
			  ->leftjoin('orders','orders.id','order_details.order_id')
			  ->select('orders.id as month', DB::raw('SUM(orders.grand_total) as total_price'))
			  ->whereBetween('orders.created_at', [$start_date, $end_date])
			  ->get(),
  
			// Current month purchase, sales, collections and dues   
			'currentMonthPurchase' => $dashboardCtrl->purchase('adminMonthly'),           
			'currentMonthSales' => $dashboardCtrl->sales('adminMonthly'),
			'currentMonthCollection' => $dashboardCtrl->collectionAmount('adminMonthly'),
			'currentMonthDue' => $dashboardCtrl->dueAmount('adminMonthly'),

			'inStockProducts' => DB::table('product_stocks')
			  ->select(DB::raw('count(	product_stocks.product_id) as stockIn'))
			  ->where([
					
					['product_stocks.qty','>', 0],
				  ]
			  )
			  ->first(),
  
			// Income 
			'allIncome'  => new AllIncomeCollection($allIncome),
				  
			// Expense
			'allExpense' => new AllExpenceCollecton($allIexpence),
  
			// Recent Sales
			'recentSales' => new RecentSalesCollection($recent_salse),
  
			// Bottom Summary
			'totalUsers' => User::select(DB::raw('count(users.id) as totalUsers'))->first(),
			'totalShops' => Shop::count(),
			  
			'totalEmployees' =>User::select(DB::raw('count(users.id) as totalEmployees'))->first(),

			'suppliers' =>Supplier::select(DB::raw('count(id) as totalSuppliers'))->first(),

			'manufacturer' => Supplier::select(DB::raw('count(id) as totalManufacturer'))->first(),

			'units' => Supplier::select(DB::raw('count(id) as totalUnits'))->first(),

			'category' =>Supplier::select(DB::raw('count(id) as totalCategory'))->first(), 
			  
			'rack' => Supplier::select(DB::raw('count(id) as totalRack'))->first(),
  
			'group' => Supplier::select(DB::raw('count(id) as totalGroups'))->first(),            
  
			  ];

			return response()->json([
				'status' => 200,
				'dashboardData' => $dashboardData
			], 200);
		
	}

	public function invoiceList(Request $request){
		
		$from = date('Y-m-d',strtotime($request->start_date));
		
		$to = date('Y-m-d',strtotime($request->end_date. ' +1 day'));
        
		$invoices = Order::whereBetween('orders.created_at', [$from, $to])->get();
		$lists = [];
		foreach ($invoices as $invoice) {

				$lists[$invoice["id"]] = array(
					'invoice_no' => $invoice["code"],
					'invoice_date' => $invoice["created_at"],
					'customer_name' => $invoice["name"],
					'phone' => $invoice["phone"],
					'total' => $invoice["grand_total"],
					'paid' => $invoice["paid_amount"],
					'due' => $invoice["due_amount"],
					'due_payment_date' => $invoice["created_at"],
					'details'=>array());
				foreach($invoice->orderDetails as $key=>$orderDetail){
					
					$lists[$invoice['id']]['details'][$key] = [
					
						'product_name' => $orderDetail->product->getTranslation('name'),
						'quantity'=> $orderDetail->quantity,
						'unit_name'=> $orderDetail->product->unit,
						'strength' => $orderDetail->variation,
						'unit_price' => $orderDetail->unit_price,
						'discount' => 0,
						'total_price' => $orderDetail->price,
						
					];
				}
		}

		$lists = array_reverse($lists);
		return response()->json([
			'status' => 200,
			'salesList' => $lists
		]);

	}

	public function todaysPurchase(Request $request)
	{
		$today = strtotime(date('Y-m-d'));
           $todays_purchase = Purchase_order_item::leftjoin('purchase_order','purchase_order_item.po_id','purchase_order.id')
		   ->leftjoin('products','purchase_order_item.product_id','products.id')
		   ->where('purchase_order.created_at','>=',$today)
		   ->select('purchase_order_item.*','products.name as productname','purchase_order.payment_amount')->get();
		  $lists = [];
		   foreach ($todays_purchase as $purchase) {
			
			$lists[$purchase["id"]] = array(
				"id" => 226,
				"shop_id"=> 1,
				"branch_id"=> 1,
				"transaction_type"=> 3,
				"transaction_id"=> null,
				"payment_method"=> 1,
				"payment_date"=> "2022-08-26",
				"total_amount"=> $purchase->amount,
				"discount"=> $purchase->discount,
				"paid_amount"=> $purchase->payment_amount,
				"due_amount"=> ($purchase->amount - $purchase->payment_amount),
				"bank_account"=> null,
				"account_number"=> null,
				"card_holder"=> null,
				"card_number"=> 0,
				"created_by"=> 1,
				"updated_by"=> 0,
				"deleted_by"=> 0,
				"created_at"=> $purchase->created_at,
				"updated_at"=> $purchase->updated_at
				);
		 }
 
		 $lists = array_reverse($lists);
		 return response()->json([
			 'status' => 200,
			 'purchaseList' => $lists
		 ]);
	}

	public function todaysPurchaseList(Request $request)
	{
		$code  = $request->get('code');

		$validatedData = $request->validate([
			'shop_id'  => 'required',
			'code'  => 'required|max:55',
			'user_id'  => 'required',
		]);

		$this->dbConnection($code);
		$token   =  getBearerToken();
		$verifyUserByToken = $this->verifiyUserByToken($token,$request->user_id);
		if (empty($verifyUserByToken)) {
			return response()->json([
				'status' => 401,
				'message' => "You are not authorized!",
			]);
		}


		$purchase_list = PurchaseDetail::leftjoin('purchase', 'purchase_detail.purchase_id', '=', 'purchase.purchase_id')
			->leftjoin('product_add', 'purchase_detail.product_id', '=', 'product_add.id')
			->select('purchase_detail.*', 'purchase_detail.id as purchaseApprovedID', 'purchase.*', 'purchase.purchase_id as purchaseId', 'purchase.discount as purchaseDiscount', 'purchase.due_amount as purchaseDue', 'product_add.name as product_name', 'purchase_detail.status as approvedStatus')
			->orderBy('purchase.created_at', 'DESC')
			->where([
				['purchase.shop_id', $verifyUserByToken->shop_id],
				['purchase.purchased_date', date('Y-m-d')],
				['purchase_detail.status', 1],
			])
			->get();

		if (!empty($purchase_list) && count($purchase_list) > 0) {
			return response()->json([
				'status' => 200,
				'purchase_list' => $purchase_list
			], 200);
		}

		return response()->json([
			'status' => 400,
			'message' => "No record found!",
		], 400);
	}

	public function todaysSales(){
       
		$today = strtotime(date('Y-m-d'));
		$today_sales = Order::where('orders.date', '>=',$today)->get();

		$lists = [];
		foreach ($today_sales as $product) {
		 $lists[$product["id"]] = array(
			"actualDue"=> 0,
			"id" => (int)explode("-",$product['code'])[1],
			"shop_id"=> 1,
			"branch_id"=> 1,
			"transaction_type"=> 3,
			"transaction_id"=> 871549,
			"payment_method"=> 1,
			"payment_date"=> $product->created_at,
			"total_amount"=> $product->grand_total,
			"discount"=> $product->coupon_discount,
			"paid_amount"=> $product->paid_amount,
			"due_amount"=> $product->due_amount,
			"bank_account"=> null,
			"account_number"=> null,
			"card_holder"=> null,
			"card_number"=> 0,
			"created_by"=> 1,
			"updated_by"=> 0,
			"deleted_by"=> 0,
			"created_at"=> $product->created_at,
			"updated_at"=> $product->updated_at
			 );
	  }

	  $lists = array_reverse($lists);
	  return response()->json([
		  'status' => 200,
		  'salesList' => $lists
	  ]);
	}


	public function salesDue(Request $request)
	{
		
		$due_lists = Order::where('due_amount','>',0 )->get();
        $list = [];

		foreach($due_lists as $due_list){
			$lists[$due_list["id"]] = array(
				"id"=> 57,
				"shop_id"=> 1,
				"sales_id"=> (int)explode("-",$due_list['code'])[1],
				"sales_type"=> 2,
				"sales_barcode"=> "",
				"invoice_date"=> $due_list->created_at,
				"customer_id"=> 57,
				"invoice_detail"=> "",
				"total_discount"=> 0,
				"sub_total"=> 0,
				"invoice_discount"=> $due_list->coupon_discount,
				"grand_total"=> $due_list->grand_total,
				"paid_amount"=> $due_list->paid_amount,
				"due_amount"=> $due_list->due_amount,
				"is_due"=> 1,
				"due_payment_date"=> "2022-08-16",
				"change_amount"=> 0,
				"created_by"=> 1,
				"month"=> 8,
				"year"=> 2022,
				"hold_title"=> null,
				"hold_status"=> 0,
				"updated_by"=> 1,
				"deleted_by"=> 0,
				"created_at"=> "2022-08-16 12=>53=>50",
				"updated_at"=> "2022-08-16 12=>53=>50",
				"purchase_upload"=> null,
				"customer_name"=> $due_list->name,
				"code"=> null,
				"mobile"=> "01718837689",
				"email"=> null,
				"address"=> null
				);
		}

		$lists = array_reverse($lists);
		return response()->json([
			'status' => 200,
			'salesList' => $lists
		]);

	}
	public function outOfStock(Request $request)
	{
		$products_stock = ProductStock::leftjoin('products','product_stocks.product_id','products.id')
		->leftjoin('categories','products.category_id','categories.id')
		->leftjoin('brands','products.brand_id','brands.id')
		->select('products.*','categories.name as categoryname','brands.name as brandname')
		->where('product_stocks.qty',0)->get();
		$list = [];
		foreach ($products_stock as $product) {
		   $lists[$product["id"]] = array(
			"id"=> 2369,
			"shop_id"=> 1,
			"product_id"=> 2369,
			"quantity"=> $product->qty,
			"expiry_date"=> $product->expiry_date,
			"last_modified"=> "2020-11-24 02=>36=>10",
			"created_by"=> 27,
			"updated_by"=> 30,
			"deleted_by"=> 0,
			"created_at"=> "2020-10-08 17=>11=>19",
			"updated_at"=> "2020-11-10 04=>06=>40",
			"barcode"=> "135-0080-078",
			"generic_name"=> null,
			"name"=>$product->name,
			"code"=> $product->code,
			"box_size"=> null,
			"strength"=> $product->unit,
			"dosage_form"=> 5,
			"unit"=> null,
			"rack"=> 1,
			"group"=> 1,
			"category"=> 2,
			"dar"=> "135-0080-078",
			"manufacturer"=> 16,
			"supplier_id"=> 16,
			"trade_price"=> $product->unit_price,
			"vat_amount"=> 0,
			"total_tp"=> 5,
			"sell_price"=> $product->unit_price,
			"min_order_level"=>$product->min_qty,
			"tax"=> 0,
			"product_picture"=> "default.png",
			"side_effects"=> null,
			"description"=> null,
			"dosageForm"=> null,
			"location"=> null,
			"category_name"=> $product->categoryname,
			"manufact_name"=> "Inhouse Raw Product",
			   );
		}

		$list = array_reverse($list);
		return response()->json([
			'status' => 200,
			'currentStock' => $list
		]);
	}
	
	public function duePayment(Request $request)
	{
		$order = Order::findOrFail($request->order_id);
	         $due_amount =  $order->due_amount;
		if(!empty($order->payment_details)){
			$orderpayment = json_decode($order->payment_details);
			if(!empty($orderpayment)){
			  $paid = $orderpayment->amount + $request->payment_amount;
			}
		  }else{
			$paid = $request->payment_amount;
		  }
		 
		$oVal = (object)[
		 'amount'=>$paid,
		 'status'=>'VALID',
		 'error'=>null
	 ];
 
		
		$order->payment_status_viewed = '1';
		$order->paid_amount = $paid;
		$order->due_amount = $due_amount-$paid;
		
		if($paid > 0 ){
			if($order->grand_total == $paid){
				$order->payment_status = "Paid";
			}else{
				$order->payment_status = "Partial";
			}
		}
		$order->payment_details = json_encode($oVal);
		$order->save();
 
		if ($order->payment_status == 'Paid' || $order->payment_status == 'Partial') {
			$array['view'] = 'emails.payment';
			$array['subject'] = translate('Your order payment has been paid') . ' - ' . $order->code;
			$array['from'] = 'sales@4pos.com';
			$array['order'] = $order;
			$shipping_address = json_decode($order->shipping_address);
			if(!empty($shipping_address->email)){
				//Mail::to($shipping_address->email)->queue(new InvoiceEmailManager($array));
			}
 
			if(!empty($order->user_id)){
				$customer_id = $order->user_id;
			}else{
				$customer_id = $order->guest_id;
			}
			$cust_ledger = array();
			$cust_ledger['customer_id'] =$customer_id;
			$cust_ledger['order_id'] =$request->order_id;
			$cust_ledger['descriptions'] ='Cash Payment';
			$cust_ledger['type'] ='Payment';
			$cust_ledger['debit'] =0;
			$cust_ledger['credit'] =$request->payment_amount;
			$cust_ledger['date'] =date('Y-m-d',strtotime($request->payment_date));
			save_customer_ledger($cust_ledger);
		}
		
	return response()->json([
		'status' => 200,
		'message'   => "Thanks! Your record has added successfully!",
	]);

	return response()->json([
		'status' => 422,
		'message' => "We cannot process your data due to data quality issues!",
	]);
	
	}
}
