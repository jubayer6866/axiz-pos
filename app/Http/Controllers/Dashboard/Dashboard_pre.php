<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Settings\Location\Address;
use App\Http\Controllers\Purchase\PurchaseManagement;
use App\Model\Modules\Settings\Location\Division;
use App\Model\Modules\HRM\EmployeeRecords;
use App\Model\Modules\HRM\UserSetup;
use App\Model\Modules\Product\ProductCategory;
use App\Model\Modules\Product\ProductGroup;
use App\Model\Modules\Product\ProductManufacturer;
use App\Model\Modules\Product\ProductOrigin;
use App\Model\Modules\Product\ProductRack;
use App\Model\Modules\Product\ProductSetup;
use App\Model\Modules\Product\ProductUnits;
use App\Model\Modules\Product\ProductSupplier;
use App\Model\Modules\Stock\CurrentStock;
use App\Model\Modules\Accounts\IncomeModel;
use App\Model\Modules\Accounts\ExpenseModel;
use App\Model\Modules\Sales\Sales;
use App\Model\Modules\Settings\ShopInfo;
use Session;
use DB;
use Carbon\Carbon;

class Dashboard extends Controller
{
  protected $path = "modules.dashboard.";

  // Constructor for all methods  
  public function __construct()
  {
    $this->yesterday = date('Y-m-d', strtotime("-1 days"));
    $this->today     = Carbon::today();
    $this->month     = date('m');
    $this->year      = date('Y');
  }
  /**
   * Support ticket
   */
  public function supportTicket()
  {
    return view('modules.support.index');
  }
  public function desktop_software()
  {
    return view('modules.support.desktop_software');
  }
  /**
   * @return dashboard global calculation
   */

  // Purchase
  public function purchase($purchaseType)
  {

    if ($purchaseType == 'adminTodays') { // admin or superadmin dashboard

      return DB::table('purchase_detail')
        ->leftjoin('purchase', 'purchase.purchase_id', 'purchase_detail.purchase_id')
        ->select(DB::raw('SUM(purchase_detail.total_price) as todays_purchase'), DB::raw('SUM(purchase.discount) as todaysDiscount'))
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.purchased_date', $this->today],
          ['purchase_detail.status', 1],
          ['purchase.approve_status', 1],
        ])
        ->first();
    } else if ($purchaseType == 'salesManTodays') { // Sales dashboard today's purchase

      return DB::table('purchase_detail')
        ->leftjoin('purchase', 'purchase.purchase_id', 'purchase_detail.purchase_id')
        ->select(DB::raw('SUM(purchase_detail.total_price) as todays_purchase'), DB::raw('SUM(purchase.discount) as todaysDiscount'))
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.purchased_date', $this->today],
          ['purchase_detail.status', 1],
          ['purchase.approve_status', 1],
          ['purchase.created_by', Session::get('user_id')],
        ])
        ->first();
    } else if ($purchaseType == 'salesManYesterday') { // Sales dashboard yesterday's purchase

      return DB::table('purchase_detail')
        ->leftjoin('purchase', 'purchase.purchase_id', 'purchase_detail.purchase_id')
        ->select(DB::raw('SUM(purchase_detail.total_price) as yesterdayPurchase'), DB::raw('SUM(purchase.discount) as yesterdayDiscount'))
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.purchased_date', $this->yesterday],
          ['purchase_detail.status', 1],
          ['purchase.approve_status', 1],
          ['purchase.created_by', Session::get('user_id')],
        ])
        ->first();
    } else if ($purchaseType == 'adminMonthly') { // admin or superadmin monthly purchase

      return DB::table('purchase_detail')
        ->leftjoin('purchase', 'purchase.purchase_id', 'purchase_detail.purchase_id')
        ->select('purchase.month', DB::raw('SUM(purchase_detail.total_price) as total_price'))
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.month', date('m')],
          ['purchase.year', date('Y')],
          ['purchase_detail.status', 1],
        ])
        ->groupBy('purchase.month')
        ->get();
    } else if ($purchaseType == 'salesManMonthly') { // Sales dashboard monthly purchase

      return DB::table('purchase_detail')
        ->leftjoin('purchase', 'purchase.purchase_id', 'purchase_detail.purchase_id')
        ->select(DB::raw('SUM(purchase_detail.total_price) as total_price'))
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.month', date('m')],
          ['purchase.year', date('Y')],
          ['purchase_detail.status', 1],
          ['purchase.created_by', Session::get('user_id')],
        ])
        ->first();
    }
  }

  // Sales
  public function sales($salesType)
  {

    if ($salesType == 'adminTodays') { // admin or superadmin dashboard todays sales

      return DB::table('sales')
        ->where([
          ['hold_status', 0],
          ['shop_id', Session::get('shop_id')],
        ])
        ->whereDate('sales.invoice_date', Carbon::today())
        ->first(array(
          DB::raw('SUM(sales.grand_total) as totalSales')
        ));
    } else if ($salesType == 'salesManTodays') { // Sales dashboard today's sales

      return DB::table('sales')
        ->where([
          ['hold_status', 0],
          ['shop_id', Session::get('shop_id')],
          ['created_by', Session::get('user_id')],
        ])
        ->whereDate('sales.invoice_date', Carbon::today())
        ->first(array(
          DB::raw('SUM(sales.grand_total) as totalSales')
        ));
    } else if ($salesType == 'salesManYesterday') { // Sales dashboard yesterday's sales

      return DB::table('sales')
        ->where([
          ['hold_status', 0],
          ['shop_id', Session::get('shop_id')],
          ['created_by', Session::get('user_id')],
        ])
        ->whereDate('sales.invoice_date', $this->yesterday)
        ->first(array(
          DB::raw('SUM(sales.grand_total) as totalSales')
        ));
    } else if ($salesType == 'adminMonthly') { // admin or superadmin monthly sales

      return DB::table('sales')
        ->where([
          ['shop_id', Session::get('shop_id')],
          ['month', date('m')],
          ['year', date('Y')],
          ['hold_status', 0],

        ])
        ->first(array(
          DB::raw('SUM(sales.sub_total) as monthlySales')
        ));
    } else if ($salesType == 'salesManMonthly') { // Sales dashboard monthly sales

      return DB::table('sales')
        ->where([
          ['shop_id', Session::get('shop_id')],
          ['month', date('m')],
          ['year', date('Y')],
          ['hold_status', 0],
          ['created_by', Session::get('user_id')],
        ])
        ->first(array(
          DB::raw('SUM(sales.sub_total) as monthlySales')
        ));
    }
  }

  // Recent sales
  public function recentSales($salesType)
  {

    if ($salesType != 'salesManDashboard') { // Admin recent sales

      return Sales::leftjoin('customer', 'customer.id', 'sales.customer_id')->where([
        ['sales.shop_id', Session::get('shop_id')],
        ['sales.hold_status', 0],
      ])
        ->select('sales.*', 'customer.customer_name as customerName')
        ->orderBy('sales.created_at', 'DESC')
        ->get(100);
    } else {

      return Sales::leftjoin('customer', 'customer.id', 'sales.customer_id')->where([
        ['sales.shop_id', Session::get('shop_id')],
        ['sales.hold_status', 0],
        ['sales.created_by', Session::get('user_id')],
      ])
        ->select('sales.*', 'customer.customer_name as customerName')
        ->orderBy('sales.created_at', 'DESC')
        ->get(100);
    }
  }

  // Collections
  public function collectionAmount($salesType)
  {

    if ($salesType == 'adminTodays') { // admin or superadmin dashboard todays collections

      return DB::table('payment_transactions')->leftjoin('sales', 'sales.sales_id', 'payment_transactions.transaction_id')

        ->where([
          ['payment_transactions.shop_id', Session::get('shop_id')],
          ['payment_transactions.payment_date', $this->today],
          ['sales.hold_status', 0],
          ['sales.shop_id', Session::get('shop_id')],
        ])
        ->whereIn('payment_transactions.transaction_type', [3, 4])

        ->first(array(
          DB::raw('SUM(payment_transactions.paid_amount) as todays_collection')
        ));
    } else if ($salesType == 'salesManTodays') { // Sales dashboard today's colleciton

      return DB::table('payment_transactions')->leftjoin('sales', 'sales.sales_id', 'payment_transactions.transaction_id')

        ->where([
          ['payment_transactions.shop_id', Session::get('shop_id')],
          ['payment_transactions.payment_date', $this->today],
          ['sales.hold_status', 0],
          ['sales.shop_id', Session::get('shop_id')],
          ['sales.created_by', Session::get('user_id')],
        ])
        ->whereIn('payment_transactions.transaction_type', [3, 4])
        ->first(array(
          DB::raw('SUM(payment_transactions.paid_amount) as todays_collection')
        ));
    } else if ($salesType == 'salesManYesterday') { // Sales dashboard yesterday's colleciton

      return DB::table('payment_transactions')->leftjoin('sales', 'sales.sales_id', 'payment_transactions.transaction_id')

        ->where([
          ['payment_transactions.shop_id', Session::get('shop_id')],
          ['payment_transactions.payment_date', $this->yesterday],
          ['sales.hold_status', 0],
          ['sales.shop_id', Session::get('shop_id')],
          ['sales.created_by', Session::get('user_id')],
        ])
        ->whereIn('payment_transactions.transaction_type', [3, 4])
        ->first(array(
          DB::raw('SUM(payment_transactions.paid_amount) as collection')
        ));
    } else if ($salesType == 'adminMonthly') { // admin or superadmin monthly collection

      return DB::table('payment_transactions')->leftjoin('sales', 'sales.sales_id', 'payment_transactions.transaction_id')

        ->where([
          ['payment_transactions.shop_id', Session::get('shop_id')],
          ['sales.shop_id', Session::get('shop_id')],
          ['sales.month', date('m')],
          ['sales.year', date('Y')],
          ['sales.hold_status', 0],
        ])
        ->whereIn('payment_transactions.transaction_type', [3, 4])
        ->first(array(
          DB::raw('SUM(payment_transactions.paid_amount) as monthlyCollections'),
          DB::raw('SUM(sales.due_amount) as monthlyDueAmount'),
        ));
    } else if ($salesType == 'salesManMonthly') { // Sales dashboard monthly collection

      return DB::table('payment_transactions')->leftjoin('sales', 'sales.sales_id', 'payment_transactions.transaction_id')

        ->where([
          ['payment_transactions.shop_id', Session::get('shop_id')],
          ['sales.shop_id', Session::get('shop_id')],
          ['sales.month', date('m')],
          ['sales.year', date('Y')],
          ['sales.hold_status', 0],
          ['sales.created_by', Session::get('user_id')],
        ])
        ->whereIn('payment_transactions.transaction_type', [3, 4])
        ->first(array(
          DB::raw('SUM(payment_transactions.paid_amount) as monthlyCollections'),
          DB::raw('SUM(sales.due_amount) as monthlyDueAmount'),
        ));
    }
  }

  // Dues
  public function dueAmount($salesType)
  {

    $shopId = Session::get('shop_id');
    $userId = Session::get('user_id');
    if ($salesType == 'adminTodays') { // admin or superadmin dashboard todays collections

      return DB::select("SELECT SUM(due_amount) AS dueAmount FROM sales WHERE invoice_date = '$this->today' and shop_id=$shopId");
    } else if ($salesType == 'salesManTodays') { // Sales dashboard today's due

      return DB::select("SELECT SUM(due_amount) AS dueAmount FROM sales WHERE invoice_date = '$this->today' and created_by = $userId and hold_status = 0 and shop_id=$shopId");
    } else if ($salesType == 'salesManYesterday') { // Sales dashboard yesterday's due

      return DB::select("SELECT SUM(due_amount) AS dueAmount FROM sales WHERE invoice_date = '$this->yesterday' and created_by = $userId and hold_status = 0 and shop_id=$shopId");
    } else if ($salesType == 'adminMonthly') { // admin or superadmin monthly due

      return DB::select("SELECT SUM(due_amount) AS monthlyDueAmount FROM sales WHERE hold_status = 0 and shop_id=$shopId and month=$this->month and year=$this->year");
    } else if ($salesType == 'salesManMonthly') { // Sales dashboard monthly due

      return DB::select("SELECT SUM(due_amount) AS monthlyDueAmount FROM sales WHERE hold_status = 0 and shop_id=$shopId and created_by = $userId and month=$this->month and year=$this->year");
    }
  }

  // Purchase discount
  public function purchaseDiscount($salesType)
  {

    if ($salesType == 'adminTodays') { // admin or superadmin dashboard todays collections
      return DB::table('purchase')
        ->select(DB::raw('SUM(discount) as todaysDiscount'))
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.purchased_date', $this->today],
          ['purchase.approve_status', 1],
        ])
        ->first();
    } else if ($salesType == 'salesManTodays') { // Sales dashboard today's colleciton

      return DB::table('purchase')
        ->select(DB::raw('SUM(discount) as todaysDiscount'))
        ->where([
          ['purchase.shop_id', Session::get('shop_id')],
          ['purchase.created_by', Session::get('user_id')],
          ['purchase.purchased_date', $this->today],
          ['purchase.approve_status', 1],
        ])
        ->first();
    }
  }


  /**
   * Admin dashboard
   */
  public function index(Request $request)
  {
    $shops = ShopInfo::where('status', 1)->get();
    $totalExpired = 0;
    $stockOut = 0;
    foreach ($shops as $shop) {
      $dbname = 'pharmacy_' . $shop->code;
      \Config::set('database.connections.mysql.database', $dbname);
      \DB::purge('mysql');
      // Expiry calculation
      $expiredProducts = DB::select("SELECT a.name as productName, a.strength,b.* FROM `product_add` a inner join stocks b on a.id = b.product_id");
      foreach ($expiredProducts as $info) {
        foreach (json_decode($info->batch) as $data) {
          if ($data->expiry_date < $this->today) {
            $totalExpired += $data->quantity;
          }
        }
      }

      $stk = DB::table('currentstock')
        ->select(DB::raw('count(currentstock.product_id) as stock_out'))
        ->where([
          ['currentstock.quantity', '=', 0],
        ])
        ->first();
      $stockOut += ($stk->stock_out) ? $stk->stock_out : 0;
    }

    return view('modules.dashboard.pms-dashboard', [
      // Stockout  
      'stockOut' => $stockOut,
      // Expired products  
      'expiredProducts' => $totalExpired,
      'totalShops' => count($shops),


    ]);
  }

  /**
   * Sales dashboard 
   */
  public function salesDashboard(Request $request)
  {

    return view('modules.dashboard.sales-dashboard', [

      // Todays purchase, sales, collections, dues
      'todaysPurchase' => $this->purchase('salesManTodays'),
      'todaysPurchaseDiscount' => $this->purchaseDiscount('salesManTodays'),
      'todaysSales' => $this->sales('salesManTodays'),
      'todaysCollection' => $this->collectionAmount('salesManTodays'),
      'todaysDue' => $this->dueAmount('salesManTodays'),

      // Yesterday's purchase, sales, collections, dues
      'yesterdaysPurchase' => $this->purchase('salesManYesterday'),
      'yesterdaysPurchaseDiscount' => $this->purchaseDiscount('salesManYesterday'),
      'yesterdaysSales' => $this->sales('salesManYesterday'),
      'yesterdaysCollection' => $this->collectionAmount('salesManYesterday'),
      'yeaterdaysDue' => $this->dueAmount('salesManYesterday'),

      // Monthly purchase, sales, collections and dues  
      'currentMonthPurchase' => $this->purchase('salesManMonthly'),
      'currentMonthSales' => $this->sales('salesManMonthly'),
      'currentMonthCollection' => $this->collectionAmount('salesManMonthly'),
      'currentMonthDue' => $this->dueAmount('salesManMonthly'),
      'recentSales' => $this->recentSales('salesManDashboard'),

    ]);
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function monthlyPurchaseSaledComparison()
  {
    $purchaseDetail = DB::table('purchase_detail')
      ->leftjoin('purchase', 'purchase.purchase_id', 'purchase_detail.purchase_id')
      ->select('purchase.month', DB::raw('SUM(purchase_detail.total_price) as total_price'))
      ->where([
        ['purchase.shop_id', Session::get('shop_id')],
        ['purchase.year', date('Y')],
        ['purchase_detail.status', 1],
      ])
      ->groupBy('purchase.month')
      ->get();

    return view('modules.dashboard.monthly-purchas-sale', [
      'monthlyPurchase' => $purchaseDetail
    ]);
  }
}
