<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\CommissionHistory;
use App\Models\Wallet;
use App\Models\Seller;
use App\Models\User;
use App\Models\Search;
use App\Models\Shop;
use App\Models\Supplier;
use Auth;
use DB;
use Redirect;



class ReportController extends Controller
{
    public function stock_report(Request $request)
    {
        $sort_by =null;
        $products = Product::orderBy('created_at', 'desc');
        if ($request->has('category_id')){
            $sort_by = $request->category_id;
            $products = $products->where('category_id', $sort_by);
        }
        $products = $products->paginate(15);
        return view('backend.reports.stock_report', compact('products','sort_by'));
    }

    public function in_house_sale_report(Request $request)
    {
        $sort_by =null;
        $products = Product::orderBy('num_of_sale', 'desc')->where('added_by', 'admin');
        if ($request->has('category_id')){
            $sort_by = $request->category_id;
            $products = $products->where('category_id', $sort_by);
        }
        $products = $products->paginate(15);
        return view('backend.reports.in_house_sale_report', compact('products','sort_by'));
    }

    public function seller_sale_report(Request $request)
    {
        $sort_by =null;
        // $sellers = User::where('user_type', 'seller')->orderBy('created_at', 'desc');
        $sellers = Shop::with('user')->orderBy('created_at', 'desc');
        if ($request->has('verification_status')){
            $sort_by = $request->verification_status;
            $sellers = $sellers->where('verification_status', $sort_by);
        }
        $sellers = $sellers->paginate(10);
        return view('backend.reports.seller_sale_report', compact('sellers','sort_by'));
    }

    public function wish_report(Request $request)
    {
        $sort_by =null;
        $products = Product::orderBy('created_at', 'desc');
        if ($request->has('category_id')){
            $sort_by = $request->category_id;
            $products = $products->where('category_id', $sort_by);
        }
        $products = $products->paginate(10);
        return view('backend.reports.wish_report', compact('products','sort_by'));
    }

    public function user_search_report(Request $request){
        $searches = Search::orderBy('count', 'desc')->paginate(10);
        return view('backend.reports.user_search_report', compact('searches'));
    }
    
    public function commission_history(Request $request) {
        $seller_id = null;
        $date_range = null;
        
        if(Auth::user()->user_type == 'seller') {
            $seller_id = Auth::user()->id;
        } if($request->seller_id) {
            $seller_id = $request->seller_id;
        }
        
        $commission_history = CommissionHistory::orderBy('created_at', 'desc');
        
        if ($request->date_range) {
            $date_range = $request->date_range;
            $date_range1 = explode(" / ", $request->date_range);
            $commission_history = $commission_history->where('created_at', '>=', $date_range1[0]);
            $commission_history = $commission_history->where('created_at', '<=', $date_range1[1]);
        }
        if ($seller_id){
            
            $commission_history = $commission_history->where('seller_id', '=', $seller_id);
        }
        
        $commission_history = $commission_history->paginate(10);
        if(Auth::user()->user_type == 'seller') {
            return view('seller.reports.commission_history_report', compact('commission_history', 'seller_id', 'date_range'));
        }
        return view('backend.reports.commission_history_report', compact('commission_history', 'seller_id', 'date_range'));
    }
    
    public function wallet_transaction_history(Request $request) {
        $user_id = null;
        $date_range = null;
        
        if($request->user_id) {
            $user_id = $request->user_id;
        }
        
        $users_with_wallet = User::whereIn('id', function($query) {
            $query->select('user_id')->from(with(new Wallet)->getTable());
        })->get();

        $wallet_history = Wallet::orderBy('created_at', 'desc');
        
        if ($request->date_range) {
            $date_range = $request->date_range;
            $date_range1 = explode(" / ", $request->date_range);
            $wallet_history = $wallet_history->where('created_at', '>=', $date_range1[0]);
            $wallet_history = $wallet_history->where('created_at', '<=', $date_range1[1]);
        }
        if ($user_id){
            $wallet_history = $wallet_history->where('user_id', '=', $user_id);
        }
        
        $wallets = $wallet_history->paginate(10);

        return view('backend.reports.wallet_history_report', compact('wallets', 'users_with_wallet', 'user_id', 'date_range'));
    }

    public function supplier_ledger(Request $request)
    {
        $sort_by = null;
        $status = null;
        $start_date = date('Y-m-01');
        $end_date = date('Y-m-t');
		if(empty($request->start_date))
			$request->start_date = $start_date;
		if(empty($request->end_date))
			$request->end_date = $end_date;
        $cust = array();
        $orders = array();

        
    $sql = "SELECT
        s.id,s.name,sum(sl.debit) as debit,sum(sl.credit) as credit,sum(sl.balance) as balance,
        (select sum(sll.debit-sll.credit) from supplier_ledger as sll where s.id = sll.supplier_id and sll.date <'".$request->start_date."') as opening_balance
    FROM
        suppliers s
        LEFT JOIN supplier_ledger sl ON s.id = sl.supplier_id";
        
         if (!empty($request->start_date) && !empty($request->end_date)) {
            $start_date = date('Y-m-d',strtotime($request->start_date));
            $end_date = date('Y-m-d',strtotime($request->end_date));
            $sql.="	where sl.date between '".$start_date."' and '".$end_date."' or sl.date is null ";
        }else{
            $sql.="	where sl.date between '".$start_date."' and '".$end_date."' or sl.date is null ";
		}
        $sql.=" and sl.debit != '0' and sl.credit != '0'
        GROUP BY s.id
        order by s.name asc";
        $customers = DB::select($sql);
        return view('backend.reports.supplier_ledger_main', compact('customers', 'start_date', 'end_date'));
    }

    public function supplier_ledger_details(Request $request)
    {
        $cust_id = $request->cust_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $cust = Supplier::where('id', $cust_id)->first();
        $sql = "SELECT
        s.name,p.purchase_no,sl.purchase_id,sl.date,sl.type,sl.descriptions,s.id,sl.debit as debit,sl.credit as credit,sl.balance as balance
    FROM
        suppliers s
        LEFT JOIN supplier_ledger sl ON s.id = sl.supplier_id
        LEFT JOIN purchase_order p ON sl.id = p.id";
        
            $sql.="	where s.id=$cust_id and sl.date between '".$start_date."' and '".$end_date."'";
        
        $sql.=" order by sl.date,sl.id asc";
        $customers = DB::select($sql);


        $sql = "SELECT sum(sl.debit-sl.credit) as opening_balance
        FROM
            suppliers s
            LEFT JOIN supplier_ledger sl ON s.id = sl.supplier_id";
            $sql.="	where s.id=$cust_id and sl.date < '".$start_date."'";
            
            $opening = DB::select($sql);
                return view('backend.reports.supplier_ledger', compact('customers', 'cust','start_date', 'end_date','opening'));
    }


    //customre ledger start 

    public function customer_ledger(Request $request)
    {
         $sort_by = null;
        $status = null;

		$sort_search = '';

        $start_date = date('Y-m-01');
        $end_date = date('Y-m-t');

		if(empty($request->start_date))
			$request->start_date = $start_date;
		if(empty($request->end_date))
			$request->end_date = $end_date;
            
        $cust = array();
        $orders = array();

        $sql = "SELECT
        u.id as customer_no,u.name,sum(cl.debit) as debit,sum(cl.credit) as credit,sum(cl.balance) as balance,
   (select sum(cll.debit-cll.credit) from customer_ledger as cll where u.id=cll.customer_id and cll.date < '".$request->start_date."') as opening_balance
FROM
        users u
        LEFT JOIN customer_ledger cl ON u.id = cl.customer_id";
		$sql.=" where 1=1 ";
         if (!empty($request->start_date) && !empty($request->end_date)) {
            $start_date = date('Y-m-d',strtotime($request->start_date));
            $end_date = date('Y-m-d',strtotime($request->end_date));
            $sql.="	and (cl.date between '".$start_date."' and '".$end_date."' or cl.date is null) ";
        }
		if ($request->has('search')){
			$sort_search = $s = $request->search;
			 $sql.=" and (u.email like '%".$s."%' or u.name like '%".$s."%' or u.phone like '%".$s."%') ";
		}
        $sql.="	and (debit>0 or credit>0) 
        GROUP BY u.id
        order by u.name asc";
        $customers = DB::select($sql);
        return view('backend.reports.customer_ledger_main', compact('customers', 'start_date', 'end_date','sort_search'));
    }

    public function customer_ledger_details(Request $request)
    {
        $cust_id = $request->cust_id;
		if(empty($cust_id))
			$cust_id = $request->customer_id;
		if(empty($cust_id))
			return Redirect::back();
			
        $start_date = !empty($request->start_date) ? $request->start_date : date('Y-m-01');
        $end_date = !empty($request->end_date) ? $request->end_date : date('Y-m-t');

        $cust = User::where('id', $cust_id)->first();
        $sql = "SELECT
        u.id as customer_no,u.name,o.code,cl.order_id,cl.date,cl.type,cl.descriptions,u.id,cl.debit as debit,cl.credit as credit,cl.balance as balance
    FROM
        users u
        LEFT JOIN customer_ledger cl ON u.id = cl.customer_id
        LEFT JOIN orders o ON cl.order_id = o.id";
            $sql.="	where u.id=$cust_id and cl.date between '".$start_date."' and '".$end_date."'";
        
        $sql.=" order by cl.date asc,cl.order_id DESC,
		 CASE 
			WHEN cl.type='Order' THEN 1 
			WHEN cl.type='Discount' THEN 2
			WHEN cl.type='Payment' THEN 3	
		 END ASC 
		";
		
        $customers = DB::select($sql);
		
		$sql = "SELECT sum(cl.debit-cl.credit) as opening_balance
    FROM
        users u
	LEFT JOIN customer_ledger cl ON u.id = cl.customer_id";
        $sql.="	where u.id=$cust_id and cl.date < '".$start_date."'";
        
        $opening = DB::select($sql);
		
        return view('backend.reports.customer_ledger', compact('customers', 'cust','start_date', 'end_date','opening'));
    }
}
