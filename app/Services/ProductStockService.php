<?php

namespace App\Services;

use App\Models\ProductStock;
use App\Utility\ProductUtility;
use Combinations;

class ProductStockService
{
    public function store(array $data, $product)
    {
        $collection = collect($data);
        //dd($collection);
        $options = ProductUtility::get_attribute_options($collection);
        
        
        //Generates the combinations of customer choice options
        $combinations = Combinations::makeCombinations($options);
       
        $pto_stocks = ProductStock::where(['product_id'=>$product->id])->get();
        
        $variant = '';
        if (count($combinations[0]) > 0) {
            $product->variant_product = 1;
            $product->save();
            foreach ($combinations as $key => $combination) {
                $str = ProductUtility::get_combination_string($combination, $collection);
                
                $product_stock = ProductStock::where(['product_id'=>$product->id,'variant'=>$str])->first();
               
                $batchArray[] = array(
                    'purchase_id'      => 0,
                    'batch_no'         =>  0,
                    'quantity'         =>  request()['qty_' . str_replace('.', '_', $str)],
                    'expiry_date'         =>  0,
                    'unit_price'       =>  request()['price_' . str_replace('.', '_', $str)],
                    'damage_status'    => 0, // default 0 for not damage and 1 for damaged
                  );
                  
                  $batch = json_encode($batchArray);
               

                 if(!empty($product_stock)){
                    $product_stock->variant = $str;
                    $product_stock->batch_no = request()['batch_' . str_replace('.', '_', $str)];
                    $product_stock->price = request()['price_' . str_replace('.', '_', $str)];
                    $product_stock->sku = request()['sku_' . str_replace('.', '_', $str)];
                    $product_stock->qty = request()['qty_' . str_replace('.', '_', $str)];
                    $product_stock->image = request()['img_' . str_replace('.', '_', $str)];
                    $product_stock->update();

                }else{

                    $product_stock = new ProductStock();
                    $product_stock->product_id = $product->id;
                    $product_stock->variant = $str;
                    $product_stock->batch_no = request()['batch_' . str_replace('.', '_', $str)];
                    $product_stock->price = request()['price_' . str_replace('.', '_', $str)];
                    $product_stock->sku = request()['sku_' . str_replace('.', '_', $str)];
                    $product_stock->qty = request()['qty_' . str_replace('.', '_', $str)];
                    $product_stock->image = request()['img_' . str_replace('.', '_', $str)];
                    $product_stock->save();

                }
               
            }
        } else {

            $product_stock = ProductStock::where(['product_id'=>$product->id])->first();
            
            $batchArray[] = array(
                'purchase_id'      => 0,
                'batch_no'         =>  0,
                'quantity'         => $collection['current_stock'],
                'expiry_date'      => "null",
                'unit_price'       =>  $collection['unit_price'],
                'damage_status'    => 0,
              );
              $batch = json_encode($batchArray);
            
           if(!empty($product_stock)){
            $product_stock->batch_no = $collection['batch'];
            $product_stock->price = $collection['unit_price'];
            $product_stock->qty = $collection['current_stock'];
            $product_stock->update();

           }else{
              $product_stock              = new ProductStock();
              $product_stock->product_id  = $collection['product_id'];
              $product_stock->batch_no    = $collection['batch'];
              $product_stock->price       = $collection['unit_price'];
              $product_stock->qty         = $collection['current_stock'];
              $product_stock->save();
           }

        }
    }

    public function product_duplicate_store($product_stocks , $product_new)
    {
        foreach ($product_stocks as $key => $stock) {
            $product_stock              = new ProductStock;
            $product_stock->product_id  = $product_new->id;
            $product_stock->variant     = $stock->variant;
            $product_stock->price       = $stock->price;
            $product_stock->sku         = $stock->sku;
            $product_stock->qty         = $stock->qty;
            $product_stock->save();
        }
    }
}
