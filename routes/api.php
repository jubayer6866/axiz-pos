<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::apiResource('categories', 'App\Http\Controllers\Api\V2\CategoryController')->only('index');
Route::get('pos-categories', 'App\Http\Controllers\Api\V2\CategoryController@pos_category');
Route::get('sub-categories/{id}', 'App\Http\Controllers\Api\V2\SubCategoryController@index')->name('subCategories.index');
Route::get('products/category/{id}', 'App\Http\Controllers\Api\V2\ProductController@category')->name('api.products.category');


Route::get('categories/featured', 'App\Http\Controllers\Api\V2\CategoryController@featured');
Route::get('categories/home', 'App\Http\Controllers\Api\V2\CategoryController@home');
Route::get('categories/top', 'App\Http\Controllers\Api\V2\CategoryController@top');
Route::apiResource('categories', 'App\Http\Controllers\Api\V2\CategoryController')->only('index');
Route::get('sub-categories/{id}', 'App\Http\Controllers\Api\V2\SubCategoryController@index')->name('subCategories.index');


Route::post('/userLoginInfo' , 'App\Http\Controllers\Api\V2\SystemApiController@userLoginInfo');
Route::post('/getProducts' , 'App\Http\Controllers\Api\V2\SystemApiController@getProducts');
Route::post('/getProductsTest' , 'App\Http\Controllers\Api\V2\SystemApiController@getProductsTest');
Route::post('/createSales' , 'App\Http\Controllers\Api\V2\SystemApiController@createSales');
Route::post('/createSalesDemo' , 'App\Http\Controllers\Api\V2\SystemApiController@createSalesDemo');
Route::post('/getPurchaseList' , 'App\Http\Controllers\Api\V2\SystemApiController@getPurchaseList');
Route::post('/pendingPurchaseList' , 'App\Http\Controllers\Api\V2\SystemApiController@pendingPurchaseList');

Route::post('/purchaseDiscount' , 'App\Http\Controllers\Api\V2\SystemApiController@purchaseDiscount');
Route::post('/getSupplier' , 'App\Http\Controllers\Api\V2\SystemApiController@getSupplier');
Route::post('/getSupplierWiseProduct' , 'App\Http\Controllers\Api\V2\SystemApiController@getSupplierWiseProduct');
Route::post('/purchaseStore' , 'App\Http\Controllers\Api\V2\SystemApiController@purchaseStore');
Route::post('/expiryTrackingReport' , 'App\Http\Controllers\Api\V2\App\Http\Controllers\Api\V2\SystemApiController@expiryTrackingReport');
Route::post('/nearestExpiryProducts' , 'App\Http\Controllers\Api\V2\SystemApiController@nearestExpiryProducts');
Route::post('/expiredProducts' , 'App\Http\Controllers\Api\V2\SystemApiController@expiredProducts');
Route::post('/getDashboardData' , 'App\Http\Controllers\Api\V2\SystemApiController@getDashboardData');
Route::post('/todaysPurchase' , 'App\Http\Controllers\Api\V2\SystemApiController@todaysPurchase');
Route::post('/todaysPurchaseList' , 'App\Http\Controllers\Api\V2\SystemApiController@todaysPurchaseList');
Route::post('/todaysSales' , 'App\Http\Controllers\Api\V2\SystemApiController@todaysSales');
Route::post('/salesDue' , 'App\Http\Controllers\Api\V2\SystemApiController@salesDue');
Route::post('/currentStock' , 'App\Http\Controllers\Api\V2\SystemApiController@currentStock');
Route::post('/newProductStockReports' , 'App\Http\Controllers\Api\V2\SystemApiController@newProductStockReports');
Route::post('/outOfStock' , 'App\Http\Controllers\Api\V2\SystemApiController@outOfStock');
Route::post('/searchPurchaseInvoice' , 'App\Http\Controllers\Api\V2\SystemApiController@searchPurchaseInvoice');
Route::post('/transactionList' , 'App\Http\Controllers\Api\V2\SystemApiController@transactionList');
Route::post('/addTransaction' , 'App\Http\Controllers\Api\V2\SystemApiController@addTransaction');
Route::post('/roleList' , 'App\Http\Controllers\Api\V2\SystemApiController@roleList');
Route::post('/addRole' , 'App\Http\Controllers\Api\V2\SystemApiController@addRole');
Route::post('/addUser' , 'App\Http\Controllers\Api\V2\SystemApiController@addUser');
Route::post('/userList' , 'App\Http\Controllers\Api\V2\SystemApiController@userList');
Route::post('/getCategory' , 'App\Http\Controllers\Api\V2\SystemApiController@getCategory');
Route::post('/invoiceList' , 'App\Http\Controllers\Api\V2\SystemApiController@invoiceList');
Route::post('/invoiceList2' , 'App\Http\Controllers\Api\V2\SystemApiController@invoiceList2');
Route::post('/expenseList' , 'App\Http\Controllers\Api\V2\SystemApiController@expenseList');
Route::post('/incomeList' , 'App\Http\Controllers\Api\V2\SystemApiController@incomeList');

Route::post('duePayment', 'App\Http\Controllers\Api\V2\SystemApiController@duePayment');
